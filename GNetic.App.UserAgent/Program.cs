﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GNetic.App.UserAgent
{
    class Program
    {
        static void Main(string[] args)
        {
            UserAgentApp app = new UserAgentApp();
            app.Run("D:\\agents-root\\user-agent-root");
            Thread.Sleep(Timeout.Infinite);
        }
    }
}
