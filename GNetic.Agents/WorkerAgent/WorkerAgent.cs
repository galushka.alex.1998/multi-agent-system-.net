﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.layers;
using LightInject;
using GNetic.Agents.agent.communication;

namespace GNetic.Agents.WorkerAgent
{
    public class WorkerAgent : Agent
    {
        public override void Run()
        {
            IAmqpPeer amqp = Container.GetInstance<IAmqpPeer>();
            amqp.Run();
            amqp.AssertPublisher("all");
            IAgentContext context = Container.GetInstance<IAgentContext>();
            context.AgentInfo = new AgentInfo(amqp.QueueId, AgentType.WorkerAgent);
            IEnvironmentActuatorLayer actuatorLayer = Container.GetInstance<IEnvironmentActuatorLayer>();
            actuatorLayer.SendActivationSignal();
        }

        public override void Stop()
        {
            IEnvironmentActuatorLayer actuatorLayer = Container.GetInstance<IEnvironmentActuatorLayer>();
            actuatorLayer.SendDeactivationSignal();
            IAmqpPeer amqp = Container.GetInstance<IAmqpPeer>();
            amqp.Stop();
        }
    }
}
