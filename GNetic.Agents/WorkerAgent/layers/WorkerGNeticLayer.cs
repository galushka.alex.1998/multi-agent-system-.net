﻿using GNetic.Lib.gnetic;
using GNetic.Lib.logger;
using System.Collections.Generic;

namespace GNetic.Agents.WorkerAgent.layers
{
    public interface IWorkerGNeticLayer
    {
        void StartCalculation(TaskOptions task, GNeticOptions algorithmOptions, ChromosomeOptions chromosomeOptions);
        void StopCalculation();
        void ReceiveChromosomes(IEnumerable<Chromosome> chromosomes);
        Chromosome GetBestChromosome();
    }

    public class WorkerGNeticLayer : IWorkerGNeticLayer
    {
        private readonly IWorkerAgentContext Context;
        private readonly ILogger Logger;
        public WorkerGNeticLayer(IWorkerAgentContext context, ILogger logger)
        {
            Context = context;
            Logger = logger;
        }

        public Chromosome GetBestChromosome()
        {
            if (Context.Algorithm == null)
                throw new System.Exception("Algorithm does not in process state");
            var chromosome = Context.Algorithm.GetBestChromosome();
            return chromosome;
        }

        public void ReceiveChromosomes(IEnumerable<Chromosome> chromosomes)
        {
            if (Context.Algorithm == null)
                throw new System.Exception("Algorithm does not in process state");
            Context.Algorithm.ApplyExternalChromosomes(chromosomes);
        }

        public void StartCalculation(TaskOptions task, GNeticOptions algorithmOptions, ChromosomeOptions chromosomeOptions)
        {
            if (Context.Algorithm != null)
                throw new System.Exception("System job already in process!");
            Context.Algorithm = new GNeticAlgorithm(task, algorithmOptions, chromosomeOptions, Logger);
            Context.Algorithm.Run();
        }
        public void StopCalculation()
        {
            if (Context.Algorithm == null)
                throw new System.Exception("System is not in the calculation state");
            Context.Algorithm.Stop();
            Context.Algorithm = null;
        }
    }
}
