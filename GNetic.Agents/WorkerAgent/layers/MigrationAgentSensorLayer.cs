﻿using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.WorkerAgent.layers
{
    public class MigrationAgentSensorLayer :
        IEventHandler<PullChromosomeRequiredEvent>,
        IEventHandler<PushChromosomeRequiredEvent>
    {
        private readonly IWorkerBehaviorLayer BehaviorLayer;
        public MigrationAgentSensorLayer(IWorkerBehaviorLayer behaviorLayer)
        {
            BehaviorLayer = behaviorLayer;
        }
        public void Handle(PullChromosomeRequiredEvent evt)
        {
            BehaviorLayer.PullChromosomes();
        }

        public void Handle(PushChromosomeRequiredEvent evt)
        {
            BehaviorLayer.PushChromosome();
        }
    }
}
