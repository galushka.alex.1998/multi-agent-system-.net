﻿using GNetic.Agents.agent;
using GNetic.Lib.common;
using GNetic.Lib.dtos;
using GNetic.Lib.gmatic;
using GNetic.Lib.gnetic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.WorkerAgent.layers
{
    public interface IWorkerBehaviorLayer
    {
        Task PullChromosomes();
        Task PushChromosome();
        void StartCalculation(WorkerTaskDto task);
        void StopCalculation();
    }
    public class WorkerBehaviorLayer : IWorkerBehaviorLayer
    {
        private readonly IWorkerGNeticLayer GNeticLayer;
        private readonly IMigrationActuatorLayer MigrationActuator;
        private readonly IAgentContext Context;
        public WorkerBehaviorLayer(
            IAgentContext context,
            IWorkerGNeticLayer gNeticLayer,
            IMigrationActuatorLayer migrationActuator
        )
        {
            Context = context;
            GNeticLayer = gNeticLayer;
            MigrationActuator = migrationActuator;
        }



        public async Task PullChromosomes()
        {
            var migrator = GetMigrator();
            var chromosomes = await MigrationActuator.PullChromosomes(migrator.AgentId);
            GNeticLayer.ReceiveChromosomes(chromosomes);
        }

        public async Task PushChromosome()
        {
            var chromosome = GNeticLayer.GetBestChromosome();
            var migrator = GetMigrator();
            await MigrationActuator.PushChromosome(chromosome, migrator.AgentId);
        }

        public void StartCalculation(WorkerTaskDto task)
        {
            GNeticOptions algorithmOptions = new GNeticOptions()
            {
                CrossoveringChance = task.AlgorithmSettings.CrossoveringChance,
                MutationChance = task.AlgorithmSettings.MutationChance,
                InitialPopulationSize = task.AlgorithmSettings.InitialPopulationSize,
                SurviveChromosomeCount = task.AlgorithmSettings.SurviveChromosomeCount,
                OldestChromosomeAge = task.AlgorithmSettings.OldestChromosomeAge,
                MaxOldestPercent = task.AlgorithmSettings.MaxOldestPercent
            };

            ChromosomeOptions chromosomeOptions = new ChromosomeOptions()
            {
                Deviation = new Interval<double>(task.MathSettings.DeviationMin, task.MathSettings.DeviationMax),
                MathExpectation = new Interval<double>(task.MathSettings.MathExpectationMin, task.MathSettings.MathExpectationMax),
                CrossoverType = task.AlgorithmSettings.CrossoverType,
                MutationType = task.AlgorithmSettings.MutationType,
                LSystemConstraints = new Constraints()
                {
                    AxiomLength = new Interval<int>(task.LSystemSettings.AxiomLengthMin, task.LSystemSettings.AxiomLengthMax),
                    RuleCount = new Interval<int>(task.LSystemSettings.RuleCountMin, task.LSystemSettings.RuleCountMax),
                    RuleLength = new Interval<int>(task.LSystemSettings.RuleLengthMin, task.LSystemSettings.RuleLengthMax)
                }
            };
            TaskOptions taskOptions = new TaskOptions()
            {
                TimeSeries = task.TaskDescription.TimeSeries,
            };
            GNeticLayer.StartCalculation(taskOptions, algorithmOptions, chromosomeOptions);
        }
        public void StopCalculation()
        {
            GNeticLayer.StopCalculation();
        }

        private AgentInfo GetMigrator()
        {
            var migrator = Context.RemoteAgents.FirstOrDefault(a => a.AgentType == AgentType.MigrationAgent);
            if (migrator == null)
                throw new Exception("No migration agent found");
            return migrator;
        }
    }
}
