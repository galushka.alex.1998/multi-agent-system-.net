﻿using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.agent.layers;
using GNetic.Lib.gnetic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.WorkerAgent.layers
{
    public interface IMigrationActuatorLayer
    {
        Task<IEnumerable<Chromosome>> PullChromosomes(string migrationAgentId);
        Task PushChromosome(Chromosome chromosome, string migrationAgentId);

    }
    public class MigrationAgentActuatorLayer : ActuatorLayer<IAmqpPeer>, IMigrationActuatorLayer
    {
        public MigrationAgentActuatorLayer(IAmqpPeer amqp) : base(amqp)
        {

        }
        public async Task<IEnumerable<Chromosome>> PullChromosomes(string migrationAgentId)
        {
            var response = await Endpoint.Request<PullChromosomeRequest, PullChromosomeResponse>(migrationAgentId);
            var chromosomes = response.Payload.Chromosomes;
            return chromosomes;
        }

        public async Task PushChromosome(Chromosome chromosome, string migrationAgentId)
        {
            var request = new PushChromosomeRequest
            {
                Chromosome = chromosome
            };
            await Endpoint.Request<PushChromosomeRequest, PushChromosomeResponse>(request, migrationAgentId);
        }
    }
}
