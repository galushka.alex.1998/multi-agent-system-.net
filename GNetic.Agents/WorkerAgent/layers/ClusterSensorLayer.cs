﻿
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Lib.logger;
using System;
using System.Threading.Tasks;

namespace GNetic.Agents.WorkerAgent.layers
{

    public class ClusterSensorLayer : 
        IRequestHandler<WorkerStartTaskRequest, WorkerStartTaskResponse>,
        IRequestHandler<WorkerStopTaskRequest, WorkerStopTaskResponse>
    {
        private readonly IWorkerBehaviorLayer BehaviorLayer;
        private readonly ILogger Logger;
        public ClusterSensorLayer(IWorkerBehaviorLayer behaviorLayer, ILogger logger)
        {
            BehaviorLayer = behaviorLayer;
            Logger = logger;
        }

        public async Task<Response<WorkerStartTaskResponse>> Handle(Request<WorkerStartTaskRequest> request)
        {
            Logger.Log($"Start task request arrived, clusterId: {request.SenderId}");
            try
            {
                BehaviorLayer.StartCalculation(request.Payload.Task);
                return Response<WorkerStartTaskResponse>.Ok();
            }
            catch (Exception ex)
            {
                return Response<WorkerStartTaskResponse>.Fail(ex.Message);
            };
        }

        public async Task<Response<WorkerStopTaskResponse>> Handle(Request<WorkerStopTaskRequest> request)
        {
            Logger.Log($"Stop task request arrived, clusterId: {request.SenderId}");
            try
            {
                BehaviorLayer.StopCalculation();
                return Response<WorkerStopTaskResponse>.Ok();
            } catch (Exception ex)
            {
                return Response<WorkerStopTaskResponse>.Fail(ex.Message);
            }
        }
    }
}
