﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.amqp;
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.WorkerAgent.layers;
using GNetic.Lib.logger;
using LightInject;

namespace GNetic.Agents.WorkerAgent
{
    public class WorkerAgentStartup : AgentStartup
    {
        public override void Configure(IServiceContainer container, IAgentInitialConfiguration configuration)
        {
            base.Configure(container, configuration);
            container.RegisterMultiple<IAgentContext, IWorkerAgentContext, WorkerAgentContext>();
            container.RegisterMultiple<
                IRequestHandler<WorkerStartTaskRequest, WorkerStartTaskResponse>,
                IRequestHandler<WorkerStopTaskRequest, WorkerStopTaskResponse>,
                ClusterSensorLayer
                >();

            container.RegisterMultiple<
                IEventHandler<PullChromosomeRequiredEvent>,
                IEventHandler<PushChromosomeRequiredEvent>,
                MigrationAgentSensorLayer
                >();
            container.RegisterSingleton<IMigrationActuatorLayer, MigrationAgentActuatorLayer>();

            container.RegisterSingleton<IWorkerBehaviorLayer, WorkerBehaviorLayer>();
            container.RegisterSingleton<IWorkerGNeticLayer, WorkerGNeticLayer>();

            IAmqpPeer amqp = container.GetInstance<IAmqpPeer>();
            SetupAmqp(amqp, configuration);
            ILogger logger = container.GetInstance<ILogger>();
            SetupLogger(logger, configuration);
        }

        private void SetupAmqp(IAmqpPeer amqp, IAgentInitialConfiguration configuration)
        {
            AmqpConfig config = configuration.GetConfig<AmqpConfig>();
            amqp.Configure(config);
            amqp.Subscribe<WorkerStartTaskRequest, WorkerStartTaskResponse>();
            amqp.Subscribe<WorkerStopTaskRequest, WorkerStopTaskResponse>();
            amqp.Subscribe<PushChromosomeRequiredEvent>();
            amqp.Subscribe<PullChromosomeRequiredEvent>();
            amqp.Subscribe<AgentActivationEvent>();
            amqp.Subscribe<AgentDeactivationEvent>();
        }

        private void SetupLogger(ILogger logger, IAgentInitialConfiguration configuration)
        {
            LoggerConfig config = configuration.GetConfig<LoggerConfig>();
            logger.Configure(config);
        }
    }
}
