﻿using GNetic.Agents.agent;
using GNetic.Lib.gnetic;

namespace GNetic.Agents.WorkerAgent
{
    public interface IWorkerAgentContext
    {
        GNeticAlgorithm Algorithm { get; set; }
    }
    public class WorkerAgentContext : AgentContext, IWorkerAgentContext
    {
        public GNeticAlgorithm Algorithm { get; set; }
    }
}
