﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.amqp;
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.ClusterAgent.layers;
using GNetic.Agents.ClusterAgent.models;
using GNetic.Lib.logger;
using LightInject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.ClusterAgent
{
    public class ClusterAgentStartup : AgentStartup
    {
        public override void Configure(IServiceContainer container, IAgentInitialConfiguration configuration)
        {
            base.Configure(container, configuration);

            container.RegisterMultiple<IAgentContext, IClusterAgentContext, ClusterAgentContext>();
            container.RegisterSingleton<IClusterManagingLayer, ClusterManagingLayer>();
            container.RegisterSingleton<IUserAgentActuatorLayer, UserAgentActuatorLayer>();
            container.RegisterSingleton<IWorkerActuatorLayer, WorkerActuatorLayer>();

            container.RegisterMultiple<
                IRequestHandler<ClusterStartTaskRequest, ClusterStartTaskResponse>,
                IRequestHandler<ClusterStopTaskRequest, ClusterStopTaskResponse>,
                IRequestHandler<ClusterMomentResultRequest, ClusterMomentResultResponse>,
                UserAgentSensorLayer>();

            container.RegisterSingleton<
                IRequestHandler<SolutionFoundRequest, SolutionFoundResponse>,
                MigrationAgentSensorLayer
                >();

            container.RegisterSingleton<IMigrationAgentActuatorLayer, MigrationAgentActuatorLayer>();

            IAmqpPeer amqp = container.GetInstance<IAmqpPeer>();
            SetupAmqp(amqp, configuration);
            ILogger logger = container.GetInstance<ILogger>();
            SetupLogger(logger, configuration);

            IClusterAgentContext context = container.GetInstance<IClusterAgentContext>();
            context.AgentsConfiguration = configuration.GetConfig<AgentsConfiguration>();
        }

        public void SetupAmqp(IAmqpPeer amqp, IAgentInitialConfiguration configuration)
        {
            AmqpConfig config = configuration.GetConfig<AmqpConfig>();
            amqp.Configure(config);
            amqp.Subscribe<AgentActivationEvent>();
            amqp.Subscribe<AgentDeactivationEvent>();
            amqp.Subscribe<ClusterStartTaskRequest, ClusterStartTaskResponse>();
            amqp.Subscribe<ClusterStopTaskRequest, ClusterStopTaskResponse>();
            amqp.Subscribe<ClusterMomentResultRequest, ClusterMomentResultResponse>();
            amqp.Subscribe<SolutionFoundRequest, SolutionFoundResponse>();
        }

        public void SetupLogger(ILogger logger, IAgentInitialConfiguration configuration)
        {
            LoggerConfig config = configuration.GetConfig<LoggerConfig>();
            logger.Configure(config);
        }


    }
}
