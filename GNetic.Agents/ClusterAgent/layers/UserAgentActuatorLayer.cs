﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.agent.layers;
using GNetic.Lib.dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.ClusterAgent.layers
{
    public interface IUserAgentActuatorLayer
    {
        Task ProvideFoundSolution(SolutionDto solution);
    }
    public class UserAgentActuatorLayer : ActuatorLayer<IAmqpPeer>, IUserAgentActuatorLayer
    {
        private readonly IAgentContext Context;
        public UserAgentActuatorLayer(IAgentContext context, IAmqpPeer peer) : base(peer)
        {
            Context = context;
        }
        public async Task ProvideFoundSolution(SolutionDto solution)
        {
            var userAgent = GetUserAgent();
            await Endpoint.Request<SolutionFoundRequest, SolutionFoundResponse>(
                new SolutionFoundRequest
                {
                    Solution = solution
                },
                userAgent.AgentId
            );
        }

        private AgentInfo GetUserAgent()
        {
            var userAgent = Context.RemoteAgents.FirstOrDefault(a => a.AgentType == AgentType.UserAgent);
            if (userAgent == null)
                throw new Exception("No user agent found");
            return userAgent;
        }
    }
}
