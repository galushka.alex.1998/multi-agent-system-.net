﻿using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.ClusterAgent.layers
{
    public class UserAgentSensorLayer :
        IRequestHandler<ClusterStartTaskRequest, ClusterStartTaskResponse>,
        IRequestHandler<ClusterStopTaskRequest, ClusterStopTaskResponse>,
        IRequestHandler<ClusterMomentResultRequest, ClusterMomentResultResponse>
    {
        private readonly IClusterManagingLayer Cluster;
        public UserAgentSensorLayer(IClusterManagingLayer clusterManaging)
        {
            Cluster = clusterManaging;
        }

        public async Task<Response<ClusterStartTaskResponse>> Handle(Request<ClusterStartTaskRequest> request)
        {
            await Cluster.HandleStartCalculationRequest(request.Payload.Task);
            return Response<ClusterStartTaskResponse>.Ok();
        }

        public async Task<Response<ClusterStopTaskResponse>> Handle(Request<ClusterStopTaskRequest> request)
        {
            await Cluster.HandleStopCalculationRequest();
            return Response<ClusterStopTaskResponse>.Ok();
        }

        public async Task<Response<ClusterMomentResultResponse>> Handle(Request<ClusterMomentResultRequest> request)
        {
            var solution = await Cluster.GetMomentSolution();
            return Response<ClusterMomentResultResponse>.Ok(
                new ClusterMomentResultResponse { 
                    Solution = solution 
                }
            );
        }
    }
}
