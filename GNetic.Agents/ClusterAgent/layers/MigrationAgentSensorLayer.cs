﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;

namespace GNetic.Agents.ClusterAgent.layers
{
    public class MigrationAgentSensorLayer :
        IRequestHandler<SolutionFoundRequest, SolutionFoundResponse>
    {
        private readonly IClusterManagingLayer Cluster;
        public MigrationAgentSensorLayer(IClusterManagingLayer cluster)
        {
            Cluster = cluster;
        }
        public async Task<Response<SolutionFoundResponse>> Handle(Request<SolutionFoundRequest> request)
        {
            await Cluster.HandleSolutionFoundRequest(request.Payload.Solution);
            return Response<SolutionFoundResponse>.Ok();
        }
    }
}
