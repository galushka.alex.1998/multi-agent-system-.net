﻿using GNetic.Agents.agent;
using GNetic.Agents.ClusterAgent.models;
using GNetic.Lib.dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.ClusterAgent.layers
{
    public interface IClusterManagingLayer
    {
        Task HandleStartCalculationRequest(TaskDto task);
        Task HandleStopCalculationRequest();
        Task<SolutionDto> GetMomentSolution();
        Task HandleSolutionFoundRequest(SolutionDto solution);
    }
    public class ClusterManagingLayer : IClusterManagingLayer
    {
        private readonly IClusterAgentContext Context;
        private readonly IWorkerActuatorLayer WorkerActuator;
        private readonly IMigrationAgentActuatorLayer MigrationActuator;
        private readonly IUserAgentActuatorLayer UserAgentActuator;
        public ClusterManagingLayer(
            IClusterAgentContext context, 
            IWorkerActuatorLayer workerActuator,
            IMigrationAgentActuatorLayer migrationActuator,
            IUserAgentActuatorLayer userAgentActuator
        )
        {
            Context = context;
            WorkerActuator = workerActuator;
            MigrationActuator = migrationActuator;
            UserAgentActuator = userAgentActuator;
        }

        public async Task<SolutionDto> GetMomentSolution()
        {
            var migrator = GetMigrator();
            var solution = await MigrationActuator.RequestMomentResult(migrator.AgentId);
            return solution;
        }

        public async Task HandleSolutionFoundRequest(SolutionDto solution)
        {
            await UserAgentActuator.ProvideFoundSolution(solution);
            await HandleStopCalculationRequest();
        }

        public async Task HandleStartCalculationRequest(TaskDto task)
        {
            if (Context.DistributedAlgorithmState != null && Context.DistributedAlgorithmState.InProcess)
                throw new Exception("Algorithm is already in process");

            Context.DistributedAlgorithmState = new DistributedAlgorithmState
            {
                InProcess = true,
                IsCompleted = false,
                WorkerUnits = new List<WorkerUnitState>()
            };

            var migrator = GetMigrator();
            await MigrationActuator.RequestStart(task, migrator.AgentId);

            var workers = Context.RemoteAgents.Where(agent => agent.AgentType == AgentType.WorkerAgent).ToList();
            var workerConfigs = Context.AgentsConfiguration.WorkerConfigs;
            for (int i = 0; i < workers.Count; i++)
            {
                WorkerConfig config = workerConfigs.ElementAt(i % workerConfigs.Length);
                WorkerUnitState workerState = new WorkerUnitState
                {
                    WorkerId = workers[i].AgentId,
                    Settings = config
                };
                await WorkerActuator.RequestWorkerStart(workerState, task);
                Context.DistributedAlgorithmState.WorkerUnits.Add(workerState);
            }
        }

        public async Task HandleStopCalculationRequest()
        {
            if (Context.DistributedAlgorithmState == null || !Context.DistributedAlgorithmState.InProcess)
                throw new Exception("Algorithm does not in process");
            var migrator = GetMigrator();
            await MigrationActuator.RequestStop(migrator.AgentId);
            var units = Context.DistributedAlgorithmState.WorkerUnits;
            foreach(var unit in units)
                await WorkerActuator.RequestWorkerStop(unit.WorkerId);
            Context.DistributedAlgorithmState = null;
        }


        private AgentInfo GetMigrator()
        {
            var migrator = Context.RemoteAgents.FirstOrDefault(a => a.AgentType == AgentType.MigrationAgent);
            if (migrator == null)
                throw new Exception("Migrator does not exist");
            return migrator;
        }
    }
}
