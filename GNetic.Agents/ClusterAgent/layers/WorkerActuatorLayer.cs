﻿using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.agent.layers;
using GNetic.Agents.ClusterAgent.models;
using GNetic.Lib.dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.ClusterAgent.layers
{
    public interface IWorkerActuatorLayer
    {
        Task RequestWorkerStart(WorkerUnitState worker, TaskDto task);
        Task RequestWorkerStop(string workerId);
    }
    public class WorkerActuatorLayer : ActuatorLayer<IAmqpPeer>, IWorkerActuatorLayer
    {
        public WorkerActuatorLayer(IAmqpPeer amqp) : base(amqp)
        {

        }

        public async Task RequestWorkerStart(WorkerUnitState worker, TaskDto task)
        {
            WorkerStartTaskRequest req = new WorkerStartTaskRequest
            {
                Task = new WorkerTaskDto
                {
                    TaskDescription = task,
                    AlgorithmSettings = new AlgorithmSettingsDto
                    {
                        CrossoveringChance = worker.Settings.AlgorithmConfiguration.CrossoveringChance,
                        CrossoverType = (Lib.gnetic.crossover.CrossoverType)worker.Settings.AlgorithmConfiguration.CrossoverType,
                        InitialPopulationSize = worker.Settings.AlgorithmConfiguration.InitialPopulationSize,
                        MaxOldestPercent = worker.Settings.AlgorithmConfiguration.MaxOldestPercent,
                        MutationChance = worker.Settings.AlgorithmConfiguration.MutationChance,
                        MutationType = (Lib.gnetic.mutators.MutationType)worker.Settings.AlgorithmConfiguration.MutationType,
                        OldestChromosomeAge = worker.Settings.AlgorithmConfiguration.OldestAge,
                        SurviveChromosomeCount = worker.Settings.AlgorithmConfiguration.SurviveSize
                    },
                    LSystemSettings = new LSystemSettingsDto
                    {
                        AxiomLengthMax = worker.Settings.LSystemConfiguration.AxiomSizeMax,
                        AxiomLengthMin = worker.Settings.LSystemConfiguration.AxiomSizeMin,
                        RuleCountMax = worker.Settings.LSystemConfiguration.RuleCountMax,
                        RuleCountMin = worker.Settings.LSystemConfiguration.RuleCountMin,
                        RuleLengthMax = worker.Settings.LSystemConfiguration.RuleSizeMax,
                        RuleLengthMin = worker.Settings.LSystemConfiguration.RuleSizeMin
                    },
                    MathSettings = new MathSettings
                    {
                        DeviationMax = worker.Settings.MathConfiguration.DeviationMax,
                        DeviationMin = worker.Settings.MathConfiguration.DeviationMin,
                        MathExpectationMax = worker.Settings.MathConfiguration.MathExpectationMax,
                        MathExpectationMin = worker.Settings.MathConfiguration.MathExpectationMin
                    }
                }
            };
            await Endpoint.Request<WorkerStartTaskRequest, WorkerStartTaskResponse>(req, worker.WorkerId);
        }

        public async Task RequestWorkerStop(string workerId)
        {
            await Endpoint.Request<WorkerStopTaskRequest, WorkerStartTaskResponse>(workerId);
        }
    }
}
