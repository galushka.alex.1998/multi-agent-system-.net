﻿using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.agent.layers;
using GNetic.Lib.dtos;
using GNetic.Lib.logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.ClusterAgent.layers
{
    public interface IMigrationAgentActuatorLayer
    {
        Task RequestStart(TaskDto task, string migratorId);
        Task RequestStop(string migratorId);
        Task<SolutionDto> RequestMomentResult(string migratorId);
    }
    public class MigrationAgentActuatorLayer : ActuatorLayer<IAmqpPeer>, IMigrationAgentActuatorLayer
    {
        private readonly ILogger Logger;
        public MigrationAgentActuatorLayer(IAmqpPeer amqp, ILogger logger) : base(amqp)
        {
            Logger = logger;
        }
        public async Task<SolutionDto> RequestMomentResult(string migratorId)
        {
            Logger.Log($"Request to migrator for current best solution, migratorId: {migratorId}");
            var response = await Endpoint.Request<MigrationMomentResultRequest, MigrationMomentResultResponse>(migratorId);
            return response.Payload.Solution;
        }

        public async Task RequestStart(TaskDto task, string migratorId)
        {
            var req = new MigrationStartTaskRequest
            {
                Task = task
            };
            await Endpoint.Request<MigrationStartTaskRequest, MigrationStartTaskResponse>(req, migratorId);
        }

        public async Task RequestStop(string migratorId)
        {
            await Endpoint.Request<MigrationStopTaskRequest, MigrationStopTaskResponse>(migratorId);
        }
    }
}
