﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightInject;
using GNetic.Agents.agent.layers;

namespace GNetic.Agents.ClusterAgent
{
    public class ClusterAgent : Agent
    {
        public override void Run()
        {
            IAmqpPeer amqp = Container.GetInstance<IAmqpPeer>();
            amqp.Run();
            amqp.AssertPublisher("all");
            IAgentContext context = Container.GetInstance<IAgentContext>();
            context.AgentInfo = new AgentInfo(amqp.QueueId, AgentType.ClusterAgent);
            IEnvironmentActuatorLayer actuatorLayer = Container.GetInstance<IEnvironmentActuatorLayer>();
            actuatorLayer.SendActivationSignal();
        }

        public override void Stop()
        {
            IEnvironmentActuatorLayer actuatorLayer = Container.GetInstance<IEnvironmentActuatorLayer>();
            actuatorLayer.SendDeactivationSignal();
            IAmqpPeer amqp = Container.GetInstance<IAmqpPeer>();
            amqp.Stop();
        }
    }
}
