﻿using GNetic.Lib.dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.ClusterAgent.models
{

    public class DistributedAlgorithmState
    {
        public bool InProcess { get; set; } = false;
        public bool IsCompleted { get; set; } = false;
        public List<WorkerUnitState> WorkerUnits { get; set; } = new List<WorkerUnitState>();
    }

    public class WorkerUnitState
    {
        public string WorkerId { get; set; }
        public WorkerConfig Settings { get; set; }
    }
}
