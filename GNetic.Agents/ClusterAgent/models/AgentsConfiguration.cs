﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.ClusterAgent.models
{
    [JsonObject]
    public class AgentsConfiguration
    {
        [JsonProperty("worker-configs")]
        public WorkerConfig[] WorkerConfigs { get; set; }
    }
    [JsonObject]
    public class WorkerConfig
    {
        [JsonIgnore]
        public string ConfigId { get; set; } = Guid.NewGuid().ToString();
        [JsonProperty("algorithm")]
        public AlgorithmConfigModel AlgorithmConfiguration { get; set; }
        [JsonProperty("fractal-system")]
        public LSystemConfigModel LSystemConfiguration { get; set; }
        [JsonProperty("math")]
        public MathConfigModel MathConfiguration { get; set; }
    }
    [JsonObject]
    public class AlgorithmConfigModel
    {
        [JsonProperty("initial-population-size")]
        public int InitialPopulationSize { get; set; }
        [JsonProperty("survive-size")]
        public int SurviveSize { get; set; }
        [JsonProperty("crossover-chance")]
        public int CrossoveringChance { get; set; }
        [JsonProperty("crossover-type")]
        public int CrossoverType { get; set; }
        [JsonProperty("mutation-chance")]
        public int MutationChance { get; set; }
        [JsonProperty("mutation-type")]
        public int MutationType { get; set; }
        [JsonProperty("old-chromosome-age")]
        public int OldestAge { get; set; }
        [JsonProperty("max-oldest-percent")]
        public int MaxOldestPercent { get; set; }
    }
    [JsonObject]
    public class LSystemConfigModel
    {
        [JsonProperty("axiom-size-max")]
        public int AxiomSizeMax { get; set; }
        [JsonProperty("axiom-size-min")]
        public int AxiomSizeMin { get; set; }
        [JsonProperty("rule-count-max")]
        public int RuleCountMax { get; set; }
        [JsonProperty("rule-count-min")]
        public int RuleCountMin { get; set; }
        [JsonProperty("rule-size-max")]
        public int RuleSizeMax { get; set; }
        [JsonProperty("rule-size-min")]
        public int RuleSizeMin { get; set; }
    }

    [JsonObject]
    public class MathConfigModel
    {
        [JsonProperty("math-expectation-max")]
        public double MathExpectationMax { get; set; }
        [JsonProperty("math-expectation-min")]
        public double MathExpectationMin { get; set; }
        [JsonProperty("deviation-max")]
        public double DeviationMax { get; set; }
        [JsonProperty("deviation-min")]
        public double DeviationMin { get; set; }
    }
}
