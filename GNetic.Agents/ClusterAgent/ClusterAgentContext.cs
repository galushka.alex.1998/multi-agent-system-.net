﻿using GNetic.Agents.agent;
using GNetic.Agents.ClusterAgent.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.ClusterAgent
{
    public interface IClusterAgentContext : IAgentContext
    {
        DistributedAlgorithmState DistributedAlgorithmState { get; set; }
        AgentsConfiguration AgentsConfiguration { get; set; }
    }
    public class ClusterAgentContext : AgentContext, IClusterAgentContext
    {
        public DistributedAlgorithmState DistributedAlgorithmState { get; set; }
        public AgentsConfiguration AgentsConfiguration { get; set; }
    }
}
