﻿using LightInject;

namespace GNetic.Agents
{
    public static class ServiceContainerExtensions
    {

        public static void RegisterMultiple<TI1, TI2, TService>(this IServiceContainer container) 
            where TService : TI1, TI2
        {
            container.RegisterSingleton<TService>();
            container.Register(f => (TI1)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI2)container.GetInstance<TService>(), typeof(TService).FullName);
        }
        public static void RegisterMultiple<TI1, TI2, TI3, TService>(this IServiceContainer container)
            where TService : TI1, TI2, TI3
        {
            container.RegisterSingleton<TService>();
            container.Register(f => (TI1)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI2)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI3)container.GetInstance<TService>(), typeof(TService).FullName);
        }
        public static void RegisterMultiple<TI1, TI2, TI3, TI4, TService>(this IServiceContainer container)
            where TService : TI1, TI2, TI3, TI4
        {
            container.RegisterSingleton<TService>();
            container.Register(f => (TI1)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI2)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI3)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI4)container.GetInstance<TService>(), typeof(TService).FullName);
        }
        public static void RegisterMultiple<TI1, TI2, TI3, TI4, TI5, TService>(this IServiceContainer container)
            where TService : TI1, TI2, TI3, TI4, TI5
        {
            container.RegisterSingleton<TService>();
            container.Register(f => (TI1)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI2)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI3)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI4)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI5)container.GetInstance<TService>(), typeof(TService).FullName);
        }
        public static void RegisterMultiple<TI1, TI2, TI3, TI4, TI5, TI6, TService>(this IServiceContainer container)
            where TService : TI1, TI2, TI3, TI4, TI5, TI6
        {
            container.RegisterSingleton<TService>();
            container.Register(f => (TI1)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI2)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI3)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI4)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI5)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI6)container.GetInstance<TService>(), typeof(TService).FullName);
        }
        public static void RegisterMultiple<TI1, TI2, TI3, TI4, TI5, TI6, TI7, TService>(this IServiceContainer container)
            where TService : TI1, TI2, TI3, TI4, TI5, TI6, TI7
        {
            container.RegisterSingleton<TService>();
            container.Register(f => (TI1)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI2)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI3)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI4)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI5)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI6)container.GetInstance<TService>(), typeof(TService).FullName);
            container.Register(f => (TI7)container.GetInstance<TService>(), typeof(TService).FullName);
        }
    }
}
