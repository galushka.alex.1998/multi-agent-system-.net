﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GNetic.Agents.MigrationAgent.schedule
{
    public class Job
    {
        public Guid JobId { get; private set; }
        public int Interval { get; private set; }
        private bool IsRunning { get; set; }
        public Job(int interval)
        {
            Interval = interval;
            JobId = Guid.NewGuid();
        }

        public void Start(Action action)
        {
            IsRunning = true;
            Task.Run(() =>
            {
                while (IsRunning)
                {
                    Thread.Sleep(Interval);
                    action();
                }
            });
        }
        public void Stop()
        {
            IsRunning = false;
        }

        public static Job StartNew(int interval, Action action)
        {
            Job job = new Job(interval);
            job.Start(action);
            return job;
        }
    }
}
