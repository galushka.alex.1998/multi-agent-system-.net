﻿using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.agent.layers;
using GNetic.Lib.logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.MigrationAgent.layers
{
    public interface IWorkerActuatorLayer
    {
        Task SendPushRequiredEvent(string workerId);
        Task SendPullRequiredEvent(string workerId);

    }
    public class WorkerActuatorLayer : ActuatorLayer<IAmqpPeer>, IWorkerActuatorLayer
    {
        private readonly ILogger Logger;
        public WorkerActuatorLayer(IAmqpPeer amqp, ILogger logger) :base(amqp)
        {
            Logger = logger;
        }

        public async Task SendPullRequiredEvent(string workerId)
        {
            Logger.Log($"Sending pull required event to worker, workerId: {workerId}");
            var evt = new PullChromosomeRequiredEvent();
            Endpoint.Publish(evt, workerId);
        }

        public async Task SendPushRequiredEvent(string workerId)
        {
            Logger.Log($"Sending push required event to worker, workerId: {workerId}");
            var evt = new PushChromosomeRequiredEvent();
            Endpoint.Publish(evt, workerId);
        }
    }
}
