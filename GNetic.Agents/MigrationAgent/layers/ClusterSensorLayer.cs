﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Lib.logger;

namespace GNetic.Agents.MigrationAgent.layers
{
    public class ClusterSensorLayer : 
        IRequestHandler<MigrationMomentResultRequest, MigrationMomentResultResponse>,
        IRequestHandler<MigrationStartTaskRequest, MigrationStartTaskResponse>,
        IRequestHandler<MigrationStopTaskRequest, MigrationStopTaskResponse>
    {
        private readonly IMigrationLayer MigrationLayer;
        private readonly ILogger Logger;
        public ClusterSensorLayer(IMigrationLayer migrationLayer, ILogger logger)
        {
            MigrationLayer = migrationLayer;
            Logger = logger;
        }
        public async Task<Response<MigrationMomentResultResponse>> Handle(Request<MigrationMomentResultRequest> request)
        {
            Logger.Log("Moment result request arrived");
            var solution = await MigrationLayer.GetCurrentSolution();
            var response = new MigrationMomentResultResponse
            {
                Solution = solution
            };
            return Response<MigrationMomentResultResponse>.Ok(response);
        }

        public async Task<Response<MigrationStartTaskResponse>> Handle(Request<MigrationStartTaskRequest> request)
        {
            Logger.Log("Start calculation request arrived");
            await MigrationLayer.HandleStartTask(request.Payload.Task);
            return Response<MigrationStartTaskResponse>.Ok();
        }

        public async Task<Response<MigrationStopTaskResponse>> Handle(Request<MigrationStopTaskRequest> request)
        {
            Logger.Log("Stop calculation request arrived");
            await MigrationLayer.HandleStopTask();
            return Response<MigrationStopTaskResponse>.Ok();
        }
    }
}
