﻿using GNetic.Agents.MigrationAgent.schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.MigrationAgent.layers
{
    public interface IPollSchedulerLayer
    {
        Task BeginPollJob();
        Task EndPollJob();
    }
    public class PollSchedulerLayer : IPollSchedulerLayer
    {
        private readonly IMigrationAgentContext Context;
        private readonly IWorkerActuatorLayer WorkerActuator;
        private Job PullJob { get; set; }
        private Job PushJob { get; set; }
        public PollSchedulerLayer(
            IMigrationAgentContext context,
            IWorkerActuatorLayer workerActuator
        )
        {
            Context = context;
            WorkerActuator = workerActuator;
        }

        public async Task BeginPollJob()
        {
            // TODO: move intervals to agent settings
            PullJob = Job.StartNew(15000, StartPullRequiredJob);
            PushJob = Job.StartNew(15000, StartPushRequiredJob);
        }

        public async Task EndPollJob()
        {
            if (PushJob != null)
                PushJob.Stop();
            if (PullJob != null)
                PullJob.Stop();
            PushJob = null;
            PullJob = null;
        }

        private void StartPushRequiredJob()
        {
            var flow = Context.MigrationFlow;
            if (flow == null)
                return;
            var units = flow.Workers;
            foreach (var unit in units)
                WorkerActuator.SendPushRequiredEvent(unit.WorkerId);
        }

        private void StartPullRequiredJob()
        {
            var flow = Context.MigrationFlow;
            if (flow == null)
                return;
            var units = flow.Workers;
            foreach(var unit in units)
            {
                var betterUnits = from w in units
                                  where w.WorkerId != unit.WorkerId &&
                                        w.Chromosome != null &&
                                        unit.Chromosome != null ?
                                        w.Chromosome.Fitness < unit.Chromosome.Fitness :
                                        true
                                  select w;
                if (betterUnits.Count() != 0)
                    WorkerActuator.SendPullRequiredEvent(unit.WorkerId);
            }
        }
    }
}
