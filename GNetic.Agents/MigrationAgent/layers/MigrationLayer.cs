﻿using GNetic.Agents.agent;
using GNetic.Agents.MigrationAgent.models;
using GNetic.Lib.dtos;
using GNetic.Lib.gnetic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.MigrationAgent.layers
{
    public interface IMigrationLayer
    {
        Task HandleStartTask(TaskDto task);
        Task HandleStopTask();
        Task<IEnumerable<Chromosome>> HandlePullRequest(string workerId);
        Task HandlePushRequest(Chromosome chromosome, string workerId);
        Task<SolutionDto> GetCurrentSolution();
        
    }
    public class MigrationLayer : IMigrationLayer
    {
        private readonly IMigrationAgentContext Context;
        private readonly IClusterActuatorLayer Cluster;
        private readonly IWorkerActuatorLayer WorkerActuator;
        private readonly IPollSchedulerLayer PollScheduler;
        public MigrationLayer(
            IMigrationAgentContext context, 
            IClusterActuatorLayer clusterActuator, 
            IWorkerActuatorLayer workerActuator,
            IPollSchedulerLayer pollScheduler
        )
        {
            Context = context;
            Cluster = clusterActuator;
            WorkerActuator = workerActuator;
            PollScheduler = pollScheduler;
        }
        public async Task<IEnumerable<Chromosome>> HandlePullRequest(string workerId)
        {
            var worker = GetWorker(workerId);
            if (Context.MigrationFlow == null)
                throw new Exception("No calculation process tasks specified!");

            var currentState = Context.MigrationFlow.Workers.Find(w => w.WorkerId == worker.AgentId);
            var chromosomes = (
                from w in Context.MigrationFlow.Workers
                where w.WorkerId != worker.AgentId && 
                w.Chromosome != null && 
                currentState.Chromosome != null ? 
                w.Chromosome.Fitness < currentState.Chromosome.Fitness : 
                true
                select w.Chromosome
            ).ToList();
            return chromosomes;
           
        }

        public async Task HandlePushRequest(Chromosome chromosome, string workerId)
        {
            var worker = GetWorker(workerId);
            if (Context.MigrationFlow == null)
                throw new Exception("No calculation process tasks specified!");

            if (chromosome.Fitness <= Context.MigrationFlow.Task.Accuracy && Context.MigrationFlow.Durability == null)
            {
                var cluster = GetCluster();
                Context.MigrationFlow.Durability = DateTime.Now - Context.MigrationFlow.StartedOn;
                var solution = SolutionDto.FromChromosome(chromosome);
                solution.CalculationTime = Context.MigrationFlow.Durability;
                await Cluster.SendFoundResult(solution, cluster.AgentId);
                return;
            }
            var unit = Context.MigrationFlow.Workers.FirstOrDefault(w => w.WorkerId == workerId);
            if (unit.Chromosome != null && unit.Chromosome.Fitness < chromosome.Fitness)
                return;

            unit.Chromosome = chromosome;

            var units = from w in Context.MigrationFlow.Workers
                        where w.Chromosome != null ? w.Chromosome.Fitness > chromosome.Fitness : true
                        select w;
            foreach (var u in units)
                WorkerActuator.SendPullRequiredEvent(u.WorkerId);
        }

        public async Task HandleStartTask(TaskDto task)
        {
            Context.MigrationFlow = new MigrationFlow
            {
                Task = task,
                Workers = Context.RemoteAgents
                    .Where(a => a.AgentType == AgentType.WorkerAgent)
                    .Select(a => new WorkerUnitAnalyticModel
                    {
                        WorkerId = a.AgentId,
                    })
                    .ToList(),
                StartedOn = DateTime.Now
            };
            await PollScheduler.BeginPollJob();
        }

        public async Task HandleStopTask()
        {
            Context.MigrationFlow = null;
            await PollScheduler.EndPollJob();
        }

        private AgentInfo GetWorker(string workerId)
        {
            var worker = Context.RemoteAgents.FirstOrDefault(a => a.AgentId == workerId && a.AgentType == AgentType.WorkerAgent);
            if (worker == null)
                throw new Exception("Worker does not found");
            return worker;
        }

        private AgentInfo GetCluster()
        {
            var cluster = Context.RemoteAgents.FirstOrDefault(a => a.AgentType == AgentType.ClusterAgent);
            if (cluster == null)
                throw new Exception("Cluster does not found");
            return cluster;
        }

        public async Task<SolutionDto> GetCurrentSolution()
        {
            if (Context.MigrationFlow == null)
                throw new Exception("No calculation process tasks specified");

            var chromosomes = from w in Context.MigrationFlow.Workers
                              where w.Chromosome != null
                              orderby w.Chromosome.Fitness
                              select w.Chromosome;
            if (chromosomes.Count() == 0)
                throw new Exception("No chromosomes presented");

            return SolutionDto.FromChromosome(chromosomes.First());
        }
    }
}
