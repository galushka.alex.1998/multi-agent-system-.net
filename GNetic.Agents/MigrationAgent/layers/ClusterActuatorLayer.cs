﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.agent.layers;
using GNetic.Lib.dtos;
using GNetic.Lib.logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.MigrationAgent.layers
{
    public interface IClusterActuatorLayer
    {
        Task SendFoundResult(SolutionDto solution, string clusterId);
    }
    public class ClusterActuatorLayer : ActuatorLayer<IAmqpPeer>, IClusterActuatorLayer
    {
        private readonly ILogger Logger;
        public ClusterActuatorLayer(IAmqpPeer amqp, ILogger logger): base(amqp)
        {
            Logger = logger;
        }
        public async Task SendFoundResult(SolutionDto solution, string clusterId)
        {
            Logger.Log("Send found result to cluster");
            var req = new SolutionFoundRequest
            {
                Solution = solution
            };
            await Endpoint.Request<SolutionFoundRequest, SolutionFoundResponse>(req, clusterId);
        }

    }
}
