﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Lib.logger;

namespace GNetic.Agents.MigrationAgent.layers
{
    public class WorkerSensorLayer :
        IRequestHandler<PullChromosomeRequest, PullChromosomeResponse>,
        IRequestHandler<PushChromosomeRequest, PushChromosomeResponse>
    {
        private readonly IMigrationLayer MigrationLayer;
        private readonly ILogger Logger;
        public WorkerSensorLayer(IMigrationLayer migrationLayer, ILogger logger)
        {
            MigrationLayer = migrationLayer;
            Logger = logger;
        }
        public async Task<Response<PullChromosomeResponse>> Handle(Request<PullChromosomeRequest> request)
        {
            Logger.Log($"Pull request arrived: {request.SenderId}");
            var chromosomes = await MigrationLayer.HandlePullRequest(request.SenderId);
            var response = new PullChromosomeResponse
            {
                Chromosomes = chromosomes
            };
            return Response<PullChromosomeResponse>.Ok(response);
        }

        public async Task<Response<PushChromosomeResponse>> Handle(Request<PushChromosomeRequest> request)
        {
            Logger.Log($"Push request arrived: {request.SenderId}");
            await MigrationLayer.HandlePushRequest(request.Payload.Chromosome, request.SenderId);
            return Response<PushChromosomeResponse>.Ok();
        }
    }
}
