﻿using GNetic.Agents.agent;
using GNetic.Agents.MigrationAgent.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.MigrationAgent
{
    public interface IMigrationAgentContext : IAgentContext
    {
        MigrationFlow MigrationFlow { get; set; }
    }
    public class MigrationAgentContext : AgentContext, IMigrationAgentContext
    {
        public MigrationFlow MigrationFlow { get; set; }
    }
}
