﻿using GNetic.Agents.agent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightInject;
using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.layers;

namespace GNetic.Agents.MigrationAgent
{
    public class MigrationAgent : Agent
    {
        public override void Run()
        {
            IAmqpPeer amqp = Container.GetInstance<IAmqpPeer>();
            amqp.Run();
            amqp.AssertPublisher("all");
            IAgentContext context = Container.GetInstance<IAgentContext>();
            context.AgentInfo = new AgentInfo(amqp.QueueId, AgentType.MigrationAgent);
            IEnvironmentActuatorLayer actuatorLayer = Container.GetInstance<IEnvironmentActuatorLayer>();
            actuatorLayer.SendActivationSignal();
        }

        public override void Stop()
        {
            IEnvironmentActuatorLayer actuatorLayer = Container.GetInstance<IEnvironmentActuatorLayer>();
            actuatorLayer.SendDeactivationSignal();
            IAmqpPeer amqp = Container.GetInstance<IAmqpPeer>();
            amqp.Stop();
        }
    }
}
