﻿using GNetic.Lib.dtos;
using GNetic.Lib.gnetic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.MigrationAgent.models
{
    public class WorkerUnitAnalyticModel
    {
        public string WorkerId { get; set; }
        public Chromosome Chromosome { get; set; }
    }
}
