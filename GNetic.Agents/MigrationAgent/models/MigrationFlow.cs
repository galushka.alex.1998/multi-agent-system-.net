﻿using GNetic.Lib.dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.MigrationAgent.models
{
    public class MigrationFlow
    {
        public TaskDto Task { get; set; }
        public List<WorkerUnitAnalyticModel> Workers { get; set; } = new List<WorkerUnitAnalyticModel>();
        public DateTime? StartedOn { get; set; } = null;
        public TimeSpan? Durability { get; set; } = null;
    }
}
