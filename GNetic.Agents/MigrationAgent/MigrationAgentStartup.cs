﻿using GNetic.Agents.agent;
using LightInject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.MigrationAgent.layers;
using GNetic.Agents.agent.communication;
using GNetic.Lib.logger;
using GNetic.Agents.agent.communication.amqp;

namespace GNetic.Agents.MigrationAgent
{
    public class MigrationAgentStartup : AgentStartup
    {
        public override void Configure(IServiceContainer container, IAgentInitialConfiguration configuration)
        {
            base.Configure(container, configuration);
            container.RegisterMultiple<IAgentContext, IMigrationAgentContext, MigrationAgentContext>();

            // core logic
            container.RegisterSingleton<IMigrationLayer, MigrationLayer>();
            container.RegisterSingleton<IPollSchedulerLayer, PollSchedulerLayer>();
            // cluster side
            container.RegisterMultiple<
                IRequestHandler<MigrationStartTaskRequest, MigrationStartTaskResponse>,
                IRequestHandler<MigrationStopTaskRequest, MigrationStopTaskResponse>,
                IRequestHandler<MigrationMomentResultRequest, MigrationMomentResultResponse>,
                ClusterSensorLayer
                >();
            container.RegisterSingleton<IClusterActuatorLayer, ClusterActuatorLayer>();

            // worker side
            container.RegisterMultiple<
                IRequestHandler<PullChromosomeRequest, PullChromosomeResponse>,
                IRequestHandler<PushChromosomeRequest, PushChromosomeResponse>,
                WorkerSensorLayer
                >();
            container.RegisterSingleton<IWorkerActuatorLayer, WorkerActuatorLayer>();

            IAmqpPeer amqp = container.GetInstance<IAmqpPeer>();
            SetupAmqp(amqp, configuration);
            ILogger logger = container.GetInstance<ILogger>();
            SetupLogger(logger, configuration);
        }

        private void SetupAmqp(IAmqpPeer amqp, IAgentInitialConfiguration configuration)
        {
            AmqpConfig config = configuration.GetConfig<AmqpConfig>();
            amqp.Configure(config);

            amqp.Subscribe<MigrationStartTaskRequest, MigrationStartTaskResponse>();
            amqp.Subscribe<MigrationStopTaskRequest, MigrationStopTaskResponse>();
            amqp.Subscribe<MigrationMomentResultRequest, MigrationMomentResultResponse>();
            amqp.Subscribe<PullChromosomeRequest, PullChromosomeResponse>();
            amqp.Subscribe<PushChromosomeRequest, PushChromosomeResponse>();

            amqp.Subscribe<AgentActivationEvent>();
            amqp.Subscribe<AgentDeactivationEvent>();
        }

        private void SetupLogger(ILogger logger, IAgentInitialConfiguration configuration)
        {
            LoggerConfig config = configuration.GetConfig<LoggerConfig>();
            logger.Configure(config);
        }
    }
}
