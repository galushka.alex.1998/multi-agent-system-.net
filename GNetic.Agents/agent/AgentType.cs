﻿

namespace GNetic.Agents.agent
{
    public enum AgentType
    {
        WorkerAgent,
        ClusterAgent,
        MigrationAgent,
        UserAgent
    }
}
