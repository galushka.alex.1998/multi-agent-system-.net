﻿using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.agent.layers;
using GNetic.Lib.logger;
using LightInject;

namespace GNetic.Agents.agent
{
    public interface IAgentStartup
    {
        void Configure(IServiceContainer container, IAgentInitialConfiguration configuration);
    }

    public class AgentStartup : IAgentStartup
    {
        public virtual void Configure(IServiceContainer container, IAgentInitialConfiguration configuration)
        {
            container.RegisterInstance(container);
            container.RegisterSingleton<IAmqpPeer, AmqpPeer>();
            container.RegisterSingleton<ILogger, Logger>();
            container.RegisterMultiple<
                IEventHandler<AgentActivationEvent>,
                IEventHandler<AgentDeactivationEvent>,
                EnvironmentSensorLayer>();
            // navigation in agents environment
            container.RegisterSingleton<IEnvironmentActuatorLayer, EnvironmentActuatorLayer>();
        }


    }
}
