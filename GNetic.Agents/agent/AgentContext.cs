﻿
using System.Collections.Generic;


namespace GNetic.Agents.agent
{
    public interface IAgentContext
    {
        AgentInfo AgentInfo { get; set; }
        List<AgentInfo> RemoteAgents { get; set; }
    }
    public class AgentContext : IAgentContext
    {
        public AgentInfo AgentInfo { get; set; }
        public List<AgentInfo> RemoteAgents { get; set; } = new List<AgentInfo>();
    }
}
