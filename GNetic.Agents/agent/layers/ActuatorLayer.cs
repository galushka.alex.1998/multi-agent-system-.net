﻿using GNetic.Agents.agent.communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.agent.layers
{
    public interface IActuatorLayer<TCommunicationPeer> 
    {
        TCommunicationPeer Endpoint { get; set; }
    }
    public class ActuatorLayer<TCommunicationPeer> : IActuatorLayer<TCommunicationPeer> 
    {
        public TCommunicationPeer Endpoint { get; set; }
        public ActuatorLayer(TCommunicationPeer peer)
        {
            Endpoint = peer;
        }
    }

}
