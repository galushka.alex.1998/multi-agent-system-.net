﻿using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.events.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.agent.layers
{
    public interface IEnvironmentActuatorLayer
    {
        void SendActivationSignal();
        void SendDeactivationSignal();
    }
    public class EnvironmentActuatorLayer : ActuatorLayer<IAmqpPeer>, IEnvironmentActuatorLayer
    {
        private readonly IAgentContext Context; 
        public EnvironmentActuatorLayer(IAgentContext context, IAmqpPeer peer) : base(peer)
        {
            Context = context;
        }

        public void SendActivationSignal()
        {
            AgentActivationEvent ev = new AgentActivationEvent(Context.AgentInfo.AgentType);
            Endpoint.Publish(ev, "all");
        }

        public void SendDeactivationSignal()
        {
            AgentDeactivationEvent ev = new AgentDeactivationEvent(Context.AgentInfo.AgentType);
            Endpoint.Publish(ev, "all");
        }
    }
}
