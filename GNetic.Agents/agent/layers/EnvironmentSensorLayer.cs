﻿using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Lib.logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.agent.layers
{
    public class EnvironmentSensorLayer : 
        IEventHandler<AgentActivationEvent>,
        IEventHandler<AgentDeactivationEvent>
    {
        private readonly IAgentContext Context;
        private readonly IAmqpPeer Peer;
        private readonly IEnvironmentActuatorLayer Actuator;
        private readonly ILogger Logger;
        public EnvironmentSensorLayer(IAgentContext context, IAmqpPeer peer, IEnvironmentActuatorLayer actuator, ILogger logger)
        {
            Context = context;
            Peer = peer;
            Actuator = actuator;
            Logger = logger;
        }

        public virtual void Handle(AgentActivationEvent evt)
        {
            AgentInfo agent = Context.RemoteAgents.FirstOrDefault(a =>
                a.AgentId == evt.SenderId && a.AgentType == evt.AgentType
            );
            if (agent != null)
                return;
            agent = new AgentInfo(evt.SenderId, evt.AgentType);
            Context.RemoteAgents.Add(agent);
            Logger.Log($"Agent added: Id: {evt.SenderId} Type: {evt.AgentType}");
            Peer.AssertPublisher(evt.SenderId);
            Actuator.SendActivationSignal();
        }

        public virtual void Handle(AgentDeactivationEvent evt)
        {
            Logger.Log($"Agent removed: Id: {evt.SenderId} Type: {evt.AgentType}");
            Context.RemoteAgents = (
                from agent in Context.RemoteAgents
                where agent.AgentId != evt.SenderId && agent.AgentType != evt.AgentType
                select agent
            ).ToList();
            Peer.RejectPublisher(evt.SenderId);
        }

    }
}
