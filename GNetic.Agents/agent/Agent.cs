﻿
using LightInject;


namespace GNetic.Agents.agent
{

    public interface IAgent
    {
        void Configure<TStartup>(IAgentInitialConfiguration configuration)
            where TStartup : IAgentStartup, new();
        void Run();
        void Stop();
    }

    public abstract class Agent : IAgent
    {
        protected IServiceContainer Container; 
        public virtual void Configure<TStartup>(IAgentInitialConfiguration configuration) 
            where TStartup : IAgentStartup, new()
        {
            Container = new ServiceContainer(options => options.EnableVariance = true);
            TStartup startup = new TStartup();
            startup.Configure(Container, configuration);
        }
        public abstract void Run();
        public abstract void Stop();
    }
}
