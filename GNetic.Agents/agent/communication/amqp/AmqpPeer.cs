﻿using GNetic.Agents.agent.communication.amqp;
using GNetic.Agents.agent.communication.events;
using GNetic.Lib;
using LightInject;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace GNetic.Agents.agent.communication
{
    public interface IAmqpPeer : IConfigurable<AmqpConfig>
    {
        string QueueId { get; }
        void Subscribe<TRequest, TResponse>();
        void Subscribe<TEvent>()
            where TEvent : IIdentityEvent;
        void Publish<TEvent>(TEvent evt, string receiverId)
            where TEvent : IIdentityEvent;
        Task<Response<TResponse>> Request<TRequest, TResponse>(string receiverId)
            where TRequest : new();
        Task<Response<TResponse>> Request<TRequest, TResponse>(TRequest payload, string receiverId);
        void AssertPublisher(string publisherId);
        void RejectPublisher(string publisherId);
        void Run();
        void Stop();
    }

    public class AmqpPeer : IAmqpPeer
    {
        private string HostUrl { get; set; }
        private bool IsLocalhost { get; set; }
        private string ExchangeName { get; set; }
        private IConnection Connection { get; set; }
        private IModel Channel { get; set; }
        public string QueueId { get; private set; }
        public string CallbackQueueId { get; private set; }
        private readonly IServiceContainer ServiceContainer;

        private event EventHandler<BasicDeliverEventArgs> MessageReceived;
        private event EventHandler<BasicDeliverEventArgs> ResponseReceived;

        public AmqpPeer(IServiceContainer container)
        {
            ServiceContainer = container;
        }

        public void Run()
        {
            ConnectionFactory factory = null;
            if (IsLocalhost)
                factory = new ConnectionFactory { HostName = "localhost" };
            else
                factory = new ConnectionFactory { Uri = new Uri(HostUrl) };

            Connection = factory.CreateConnection();
            Channel = Connection.CreateModel();
            QueueId = Channel.QueueDeclare().QueueName;
            CallbackQueueId = Channel.QueueDeclare().QueueName;

            Channel.ExchangeDeclare(ExchangeName, ExchangeType.Direct);
            EventingBasicConsumer consumer = new EventingBasicConsumer(Channel);
            consumer.Received += (o, e) => MessageReceived.Invoke(o, e);
            Channel.BasicConsume(QueueId, true, consumer);

            EventingBasicConsumer callbackConsumer = new EventingBasicConsumer(Channel);
            callbackConsumer.Received += (o, e) => ResponseReceived.Invoke(o, e);
            Channel.BasicConsume(CallbackQueueId, true, callbackConsumer);

        }

        public void Stop()
        {
            if (Channel != null)
                Channel.Close();
            if (Connection != null)
                Connection.Close();
            QueueId = string.Empty;
            HostUrl = string.Empty;
            IsLocalhost = default;
            ExchangeName = string.Empty;
        }

        public void AssertPublisher(string publisherId) =>
            Channel.QueueBind(QueueId, ExchangeName, publisherId);
        public void RejectPublisher(string publisherId) =>
            Channel.QueueUnbind(QueueId, ExchangeName, publisherId);
        public void Subscribe<TEvent>()
            where TEvent : IIdentityEvent
        {
            Console.WriteLine($"Subscribe to EVENT: {typeof(TEvent).Name}");
            MessageReceived += (o, e) =>
            {
                if (e.BasicProperties.AppId == QueueId)
                    return;
                if (e.BasicProperties.Type == typeof(TEvent).Name)
                {
                    IEnumerable<IEventHandler<TEvent>> handlers = ServiceContainer.GetAllInstances<IEventHandler<TEvent>>()
                        .GroupBy(h => h.GetType().Name)
                        .Select(g => g.First())
                        .ToArray();
                    string json = Encoding.UTF8.GetString(e.Body.ToArray());
                    TEvent evt = JsonConvert.DeserializeObject<TEvent>(json);
                    foreach(var handler in handlers)
                        handler.Handle(evt);
                }
            };
        }

        public void Subscribe<TRequest, TResponse>()
        {
            Console.WriteLine($"Subscribe to REQUEST: req: {typeof(TRequest).Name}, res: {typeof(TResponse).Name}");
            MessageReceived += async (o, e) =>
            {
                if (e.BasicProperties.AppId == QueueId)
                    return;
                if (e.BasicProperties.ReplyTo == string.Empty || e.BasicProperties.CorrelationId == string.Empty)
                    return;
                if (e.BasicProperties.Type != typeof(TRequest).Name)
                    return;

                string json = Encoding.UTF8.GetString(e.Body.ToArray());
                Request<TRequest> request = JsonConvert.DeserializeObject<Request<TRequest>>(json);
                try
                {
                    IRequestHandler<TRequest, TResponse> handler = ServiceContainer.GetInstance<IRequestHandler<TRequest, TResponse>>();
                    Console.WriteLine($"Request {typeof(TRequest).Name} arrived");
                    Response<TResponse> result = await handler.Handle(request);
                    result.SenderId = QueueId;

                    string responseJson = JsonConvert.SerializeObject(result);
                    byte[] responseBody = Encoding.UTF8.GetBytes(responseJson);

                    IBasicProperties props = Channel.CreateBasicProperties();
                    props.CorrelationId = e.BasicProperties.CorrelationId;
                    props.Type = typeof(TResponse).Name;
                    Console.WriteLine($"Sending response {typeof(TResponse).Name} to {e.BasicProperties.ReplyTo}");
                    Channel.BasicPublish("", e.BasicProperties.ReplyTo, props, responseBody);
                } catch (Exception ex)
                {
                    // not able to handle request properly
                    // reasons:
                    // 1. Listener does not exist
                    // 2. Exception was thrown inside handler
                    Response<TResponse> failedResponse = new Response<TResponse>
                    {
                        Payload = default,
                        SenderId = QueueId,
                        Status = ResponseResultStatus.Failure,
                        Message = ex.Message
                    };
                    string responseJson = JsonConvert.SerializeObject(failedResponse);
                    byte[] responseBytes = Encoding.UTF8.GetBytes(responseJson);

                    IBasicProperties props = Channel.CreateBasicProperties();
                    props.CorrelationId = e.BasicProperties.CorrelationId;
                    props.Type = typeof(TResponse).Name;
                    Channel.BasicPublish("", e.BasicProperties.ReplyTo, props, responseBytes);
                }
            };
        }

        public async Task<Response<TResponse>> Request<TRequest, TResponse>(string receiverId)
            where TRequest : new()
        {
            return await Request<TRequest, TResponse>(new TRequest(), receiverId);
        }

        public Task<Response<TResponse>> Request<TRequest, TResponse>(TRequest payload, string receiverId)
        {
            TaskCompletionSource<Response<TResponse>> source = new TaskCompletionSource<Response<TResponse>>();

            Request<TRequest> request = new Request<TRequest>
            {
                SenderId = QueueId,
                Payload = payload
            };

            IBasicProperties props = Channel.CreateBasicProperties();
            props.AppId = QueueId;
            props.ReplyTo = CallbackQueueId;
            props.CorrelationId = Guid.NewGuid().ToString();
            props.Type = typeof(TRequest).Name;

            string requestJson = JsonConvert.SerializeObject(request);
            byte[] requestBytes = Encoding.UTF8.GetBytes(requestJson);

            Timer timer = new Timer();
            timer.Interval = 60000;
            timer.Elapsed += (o, e) =>
            {
                timer.Stop();
                timer.Dispose();
                Response<TResponse> failedResponse = new Response<TResponse>()
                {
                    SenderId = string.Empty,
                    Payload = default,
                    Status = ResponseResultStatus.NotReachable
                };
                source.SetException(new Exception("Request does not reach to receiver..."));
            };

            EventHandler<BasicDeliverEventArgs> responseHandler = null;
            responseHandler = (o, e) =>
            {
                if (e.BasicProperties.CorrelationId != props.CorrelationId)
                    return;
                timer.Stop();
                timer.Dispose();
                Console.WriteLine($"Response {typeof(TResponse).Name} received");
                string responseJson = Encoding.UTF8.GetString(e.Body.ToArray());
                Response<TResponse> response = JsonConvert.DeserializeObject<Response<TResponse>>(responseJson);
                if (response.Status == ResponseResultStatus.Failure)
                    source.SetException(new Exception($"Request failure. {response.Message}"));
                else 
                    source.SetResult(response);
                ResponseReceived -= responseHandler;
            };
            ResponseReceived += responseHandler;
            Console.WriteLine($"Send request {typeof(TRequest).Name} to ${receiverId}");
            Channel.BasicPublish("", receiverId, props, requestBytes);
            timer.Start();

            return source.Task;
        }

        public void Publish<TEvent>(TEvent evt, string receiverId)
            where TEvent : IIdentityEvent
        {
            IBasicProperties props = Channel.CreateBasicProperties();
            props.Type = typeof(TEvent).Name;
            props.AppId = QueueId;
            evt.SenderId = QueueId;
            byte[] payload = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(evt));
            Channel.BasicPublish(ExchangeName, receiverId, props, payload);
        }

        public void Configure(AmqpConfig config)
        {
            HostUrl = config.HostUrl;
            IsLocalhost = config.IsLocalHost;
            ExchangeName = config.ExchangeName;
        }
    }
}
