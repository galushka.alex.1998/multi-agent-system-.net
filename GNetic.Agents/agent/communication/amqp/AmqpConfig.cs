﻿using Newtonsoft.Json;

namespace GNetic.Agents.agent.communication.amqp
{
    [JsonObject]
    public class AmqpConfig
    {
        [JsonProperty("isLocalHost")]
        public bool IsLocalHost { get; set; }
        [JsonProperty("hostUrl")]
        public string HostUrl { get; set; }
        [JsonProperty("exchange-name")]
        public string ExchangeName { get; set; }
    }
}
