﻿using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.telegram;
using GNetic.Lib;
using GNetic.Lib.common;
using LightInject;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema.Generation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InputFiles;

namespace GNetic.Agents.agent.communication
{

    public interface ITelegramPeer
    {
        void Configure(TelegramConfig config);
        void Send<TMessage>(TMessage message, string messageText = "")
            where TMessage : class;
        Task<TResponse> RequestUser<TResponse>(RequestUserModel model);
        void Run();
        void Stop();
    }
    public class TelegramPeer : ITelegramPeer, IConfigurable<TelegramConfig>
    {
        private string Token { get; set; }
        private int UserName { get; set; }
        private TelegramBotClient Bot { get; set; }
        private IServiceContainer ServiceContainer { get; set; }
        private bool IsUserInteractionPipelineInProcess { get; set; }
        public TelegramPeer(IServiceContainer container)
        {
            ServiceContainer = container;
        }
        public void Run()
        {
            Bot = new TelegramBotClient(Token);
            Bot.OnMessage += async (o, e) =>
            {
                try
                {
                    if (e.Message.Chat.Id != UserName)
                        throw new Exception("You are not allowed to use system!");
                    if (IsUserInteractionPipelineInProcess)
                        return;

                    ITelegramPipelineHandler handler = ServiceContainer.GetAllInstances<ITelegramPipelineHandler>()
                        .GroupBy(h => h.GetType().Name)
                        .Select(g => g.First())
                        .ToArray()
                        .First();

                    IsUserInteractionPipelineInProcess = true;
                    string response =  await handler.HandleMessageCommand(e.Message.Text);
                    IsUserInteractionPipelineInProcess = false;
                    await Bot.SendTextMessageAsync(e.Message.Chat.Id, response);
                } catch (Exception ex)
                {
                    await Bot.SendTextMessageAsync(e.Message.Chat.Id, ex.Message);
                    IsUserInteractionPipelineInProcess = false;
                }
            };
            Bot.StartReceiving();
        }
        public void Stop()
        {
            Bot.StopReceiving();
            Bot = null;
            Token = null;
            UserName = 0;
        }

        public void Configure(TelegramConfig config)
        {
            Token = config.Token;
            UserName = config.AuthorizedUsername;
        }

        public async Task<TResponse> RequestUser<TResponse>(RequestUserModel model)
        {
            if (model.Type == TelegramUserResponseType.Boolean && typeof(TResponse).Name != typeof(bool).Name)
                throw new Exception("Boolean user request must be a bool");
            if (
                model.Type == TelegramUserResponseType.Command && 
                (typeof(TResponse).Name != typeof(string).Name || model.ExpectedCommands.Length == 0)
               )
                throw new Exception("Command user request must contains ExpectedCommand property and response type must be a string");

            ChatId chatId = new ChatId(UserName);

            TaskCompletionSource<TResponse> source = new TaskCompletionSource<TResponse>();
            EventHandler<MessageEventArgs> handler = null;
            handler = async (o, e) =>
            {
                try
                {
                    switch (model.Type)
                    {
                        case TelegramUserResponseType.Boolean:
                            string answer = e.Message.Text.ToUpper();
                            if (answer != "YES" && answer != "NO")
                                throw new Exception("Expected Yes or No.");
                            source.SetResult((TResponse)Convert.ChangeType(answer == "YES" ? true : false, TypeCode.Boolean));
                            break;
                        case TelegramUserResponseType.Command:
                            if (!model.ExpectedCommands.Contains(e.Message.Text))
                                throw new Exception($"Expected one of commands: {string.Join(",", model.ExpectedCommands)}.");
                            source.SetResult((TResponse)Convert.ChangeType(e.Message.Text, TypeCode.String));
                            break;
                        case TelegramUserResponseType.Json:
                            if (e.Message.Document == null || e.Message.Document.MimeType != "application/json")
                                throw new Exception("Expected json file!");
                            MemoryStream blob = new MemoryStream();
                            Telegram.Bot.Types.File file = await Bot.GetInfoAndDownloadFileAsync(e.Message.Document.FileId, blob);
                            string json = Encoding.UTF8.GetString(blob.ToArray());
                            try
                            {
                                TResponse response = JsonConvert.DeserializeObject<TResponse>(json);
                                source.SetResult(response);
                            } catch (Exception)
                            {
                                JSchemaGenerator generator = new JSchemaGenerator();
                                string schema = generator.Generate(typeof(TResponse)).ToString();
                                throw new Exception($"Invalid json schema! Expected schema: {schema}.");
                            }
                            break;
                    }
                    Bot.OnMessage -= handler;
                } catch (Exception ex)
                {
                    await Bot.SendTextMessageAsync(chatId, $"{ex.Message} Try again!");
                }
            };
            Bot.OnMessage += handler;
            await Bot.SendTextMessageAsync(chatId, model.Message);
            return await source.Task;
        }

        public async void Send<TMessage>(TMessage message, string messageText = "")
            where TMessage : class
        {
            ChatId chatId = new ChatId(UserName);
            if (messageText != string.Empty)
                await Bot.SendTextMessageAsync(chatId, messageText);
            if (message != null)
            {
                string json = message.ToJson();
                MemoryStream fileStream = new MemoryStream(Encoding.UTF8.GetBytes(json));
                InputOnlineFile file = new InputOnlineFile(fileStream, "message.json");
                await Bot.SendDocumentAsync(chatId, file);
            }
        }
    }
}



