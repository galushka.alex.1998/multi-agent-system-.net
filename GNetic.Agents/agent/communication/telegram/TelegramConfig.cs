﻿using Newtonsoft.Json;


namespace GNetic.Agents.agent.communication.telegram
{
    [JsonObject]
    public class TelegramConfig
    {
        [JsonProperty("bot-token")]
        public string Token { get; set; }
        [JsonProperty("authorized-username")]
        public int AuthorizedUsername { get; set; }
    }
}
