﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace GNetic.Agents.agent.communication.telegram
{
    public enum TelegramUserResponseType
    {
        Command,
        Json,
        Boolean
    }


    public class RequestUserModel
    {
        public TelegramUserResponseType Type { get; set; }
        public string[] ExpectedCommands { get; set; }
        public string Message { get; set; }
    }

    public interface ITelegramPipelineHandler
    {
        Task<string> HandleMessageCommand(string command);
        Task<string> HandleStartProcess();
        Task<string> HandleStopProcess();
        Task<string> HandleGetResult();
    }


}
