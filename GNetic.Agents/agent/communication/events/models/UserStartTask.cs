﻿using GNetic.Lib.dtos;

namespace GNetic.Agents.agent.communication.events.models
{
    public class UserStartTaskRequest
    {
        public TaskDto Task { get; set; }
    }

    public class UserStartTaskResponse
    {

    }
}
