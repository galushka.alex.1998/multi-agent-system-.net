﻿using GNetic.Lib.gnetic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.agent.communication.events.models
{
    public class PushChromosomeRequest
    {
        public Chromosome Chromosome { get; set; }
    }

    public class PushChromosomeResponse
    {

    }
}
