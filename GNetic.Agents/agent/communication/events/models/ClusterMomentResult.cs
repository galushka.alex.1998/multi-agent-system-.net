﻿

using GNetic.Lib.dtos;

namespace GNetic.Agents.agent.communication.events.models
{
    public class ClusterMomentResultRequest
    {

    }

    public class ClusterMomentResultResponse
    {
        public SolutionDto Solution { get; set; }
    }
}
