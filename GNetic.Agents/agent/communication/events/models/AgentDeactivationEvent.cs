﻿

namespace GNetic.Agents.agent.communication.events.models
{
    public class AgentDeactivationEvent : IdentityEvent
    {
        public AgentType AgentType { get; set; }
        public AgentDeactivationEvent(AgentType agentType)
        {
            AgentType = agentType;
        }
    }
}
