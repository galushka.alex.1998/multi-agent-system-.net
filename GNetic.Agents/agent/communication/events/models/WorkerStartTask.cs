﻿using GNetic.Lib.dtos;

namespace GNetic.Agents.agent.communication.events.models
{
    public class WorkerStartTaskRequest
    {
        public WorkerTaskDto Task { get; set; }
    }

    public class WorkerStartTaskResponse
    {

    }
}
