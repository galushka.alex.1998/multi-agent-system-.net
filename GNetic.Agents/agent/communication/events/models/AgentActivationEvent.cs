﻿
namespace GNetic.Agents.agent.communication.events.models
{
    public class AgentActivationEvent : IdentityEvent
    {
        public AgentType AgentType { get; set; }
        public AgentActivationEvent(AgentType agentType)
        {
            AgentType = agentType;
        }
    }
}
