﻿using GNetic.Lib.dtos;


namespace GNetic.Agents.agent.communication.events.models
{
    public class SolutionFoundRequest
    {
        public SolutionDto Solution { get; set; }
    }

    public class SolutionFoundResponse
    {

    }
}
