﻿using GNetic.Lib.dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Agents.agent.communication.events.models
{
    public class MigrationMomentResultRequest
    {
    }

    public class MigrationMomentResultResponse
    {
        public SolutionDto Solution { get; set; }
    }
}
