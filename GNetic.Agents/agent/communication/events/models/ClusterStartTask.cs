﻿using GNetic.Lib.dtos;


namespace GNetic.Agents.agent.communication.events.models
{
    public class ClusterStartTaskRequest 
    {
        public TaskDto Task { get; set; }
    }

    public class ClusterStartTaskResponse
    {

    }
}
