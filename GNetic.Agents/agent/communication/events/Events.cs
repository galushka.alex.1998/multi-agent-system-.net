﻿

using System.Threading.Tasks;

namespace GNetic.Agents.agent.communication.events
{
    public interface IEvent
    { 
    }
    public class Event : IEvent
    {
    }
    public interface IIdentityEvent : IEvent
    {
        string SenderId { get; set; }
    }
    public class IdentityEvent : Event, IIdentityEvent
    {
        public string SenderId { get; set; }
    }

    public enum ResponseResultStatus
    {
        Ok,
        Failure,
        NotReachable,
    }

    public interface IResponse<TPayload>
    {
        string SenderId { get; set; }
        TPayload Payload { get; set; }
    }

    public class Response<TPayload> : IResponse<TPayload>
    {
        public string SenderId { get; set; }
        public ResponseResultStatus Status { get; set; }
        public string Message { get; set; }
        public TPayload Payload { get; set; }

        public static Response<TPayload> Ok(string message = "")
        {
            return new Response<TPayload>
            {
                Status = ResponseResultStatus.Ok,
                Message = message,
                Payload = default
            };
        }

        public static Response<TPayload> Ok(TPayload payload, string message = "")
        {
            return new Response<TPayload>
            {
                Status = ResponseResultStatus.Ok,
                Message = message,
                Payload = payload
            };
        }

        public static Response<TPayload> Fail(string message)
        {
            return new Response<TPayload>()
            {
                Status = ResponseResultStatus.Failure,
                Message = message,
                Payload = default
            };
        }
    }

    public interface IRequest<TPayload>
    {
        string SenderId { get; set; }
        TPayload Payload { get; set; }
    }

    public class Request<TPayload> : IRequest<TPayload>
    {
        public string SenderId { get; set; }
        public TPayload Payload { get; set; }
    }

    public interface IEventHandler<TEvent>
        where TEvent : IEvent
    {
        void Handle(TEvent evt);
    }

    public interface IRequestHandler<TRequest, TResponse>
    {
        Task<Response<TResponse>> Handle(Request<TRequest> request);
    }
}
