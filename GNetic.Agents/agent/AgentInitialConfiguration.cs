﻿
using System.Collections.Generic;

namespace GNetic.Agents.agent
{
    public interface IAgentInitialConfiguration
    {
        void SetConfig<TConfig>(TConfig config)
            where TConfig : class;
        TConfig GetConfig<TConfig>()
            where TConfig : class;
    }
    public class AgentInitialConfiguration : Dictionary<string, object>, IAgentInitialConfiguration
    {
        public TConfig GetConfig<TConfig>()
            where TConfig : class =>
            (TConfig)this[typeof(TConfig).Name];
        public void SetConfig<TConfig>(TConfig config)
            where TConfig : class =>
            Add(config.GetType().Name, config);
    }
}
