﻿using System;

namespace GNetic.Agents.agent
{
    [Serializable]
    public class AgentInfo
    {
        public string AgentId { get; set; }
        public AgentType AgentType { get; set; }
        public AgentInfo(string agentId, AgentType agentType)
        {
            AgentId = agentId;
            AgentType = agentType;
        }
    }
}
