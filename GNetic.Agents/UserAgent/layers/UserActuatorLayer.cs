﻿
using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.telegram;
using GNetic.Agents.agent.layers;
using GNetic.Lib.dtos;
using System.Threading.Tasks;

namespace GNetic.Agents.UserAgent.layers
{
    public interface IUserActuatorLayer
    {
        void SendFoundResult(SolutionDto solution);
        Task<TaskDto> GetTaskSourceData();
    }
    public class UserActuatorLayer : ActuatorLayer<ITelegramPeer>, IUserActuatorLayer
    {
        private readonly IUserAgentContext Context;
        public UserActuatorLayer(IUserAgentContext context, ITelegramPeer peer) : base(peer)
        {
            Context = context;
        }

        public async Task<TaskDto> GetTaskSourceData()
        {
            var data = await Endpoint.RequestUser<TaskDto>(new RequestUserModel
            {
                Message = "Provide source data",
                Type = TelegramUserResponseType.Json
            });
            return data;
        }

        public void SendFoundResult(SolutionDto solution)
        {
            Endpoint.Send(solution, "Solution found!");
        }
    }
}
