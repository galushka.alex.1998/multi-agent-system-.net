﻿
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Lib.logger;
using System.Threading.Tasks;

namespace GNetic.Agents.UserAgent.layers
{
    public class ClusterSensorLayer : 
        IRequestHandler<SolutionFoundRequest, SolutionFoundResponse>
    {
        private readonly IUserActuatorLayer UserActuator;
        private readonly ILogger Logger;
        public ClusterSensorLayer(IUserActuatorLayer userActuator, ILogger logger)
        {
            UserActuator = userActuator;
            Logger = logger;
        }

        public async Task<Response<SolutionFoundResponse>> Handle(Request<SolutionFoundRequest> request)
        {
            Logger.Log($"Solution found request arrived, solution: {request.Payload.Solution}");
            UserActuator.SendFoundResult(request.Payload.Solution);
            return Response<SolutionFoundResponse>.Ok();
        }
    }
}
