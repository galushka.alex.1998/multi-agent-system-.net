﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.agent.layers;
using GNetic.Lib.dtos;
using GNetic.Lib.logger;
using System.Linq;
using System.Threading.Tasks;

namespace GNetic.Agents.UserAgent.layers
{
    public interface IClusterActuatorLayer
    {
        Task StartProcess(TaskDto source);
        Task StopProcess();
        Task<SolutionDto> GetMomentResult();
    }
    public class ClusterActuatorLayer : ActuatorLayer<IAmqpPeer>, IClusterActuatorLayer
    {
        private readonly IAgentContext Context;
        private readonly ILogger Logger;
        public ClusterActuatorLayer(IAgentContext context, IAmqpPeer peer, ILogger logger) : base(peer)
        {
            Context = context;
            Logger = logger;
        }
        private AgentInfo GetClusterInfo()
        {
            AgentInfo clusterAgent = Context.RemoteAgents
                .Where(a => a.AgentType == AgentType.ClusterAgent)
                .FirstOrDefault();
            if (clusterAgent == null)
                throw new System.Exception("No cluster agent available");
            return clusterAgent;
        }
        public async Task StartProcess(TaskDto source)
        {
            Logger.Log("Sending start process request to cluster");
            var cluster = GetClusterInfo();
            ClusterStartTaskRequest req = new ClusterStartTaskRequest
            {
                Task = source
            };
            await Endpoint.Request<ClusterStartTaskRequest, ClusterStartTaskResponse>(req, cluster.AgentId);
        }

        public async Task StopProcess()
        {
            Logger.Log("Sending stop process request to cluster");
            var cluster = GetClusterInfo();
            await Endpoint.Request<ClusterStopTaskRequest, ClusterStopTaskResponse>(cluster.AgentId);
        }

        public async Task<SolutionDto> GetMomentResult()
        {
            Logger.Log("Send moment result request to cluster");
            var cluster = GetClusterInfo();
            var response = await Endpoint.Request<ClusterMomentResultRequest, ClusterMomentResultResponse>(cluster.AgentId);
            return response.Payload.Solution;
        }
    }
}
