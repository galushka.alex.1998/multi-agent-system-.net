﻿
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.agent.communication.telegram;
using GNetic.Lib.common;
using System;
using System.Threading.Tasks;

namespace GNetic.Agents.UserAgent.layers
{

    public class UserSensorLayer : 
        ITelegramPipelineHandler
    {
        private readonly IClusterActuatorLayer Cluster;
        private readonly IUserActuatorLayer UserActuator;
        public UserSensorLayer(IClusterActuatorLayer clusterActuator, IUserActuatorLayer userActuator)
        {
            Cluster = clusterActuator;
            UserActuator = userActuator;
        }

        public async Task<string> HandleMessageCommand(string command)
        {
            switch (command)
            {
                case "start":
                    return await HandleStartProcess();
                case "stop":
                    return await HandleStopProcess();
                case "get":
                    return await HandleGetResult();
                default:
                    return "Unrecognizeable command!";
            }
        }

        public async Task<string> HandleGetResult()
        {
            try
            {
                var solution = await Cluster.GetMomentResult();
                string json = solution.ToJson();
                return $"Here is a current result! {json}";
            } catch (Exception ex)
            {
                return ex.Message;
            }     
        }

        public async Task<string> HandleStartProcess()
        {
            // request data from user
            var dto = await UserActuator.GetTaskSourceData();
            await Cluster.StartProcess(dto);
            return "Calculation started!";
        }

        public async Task<string> HandleStopProcess()
        {
            try
            {
                string result = await HandleGetResult();
                await Cluster.StopProcess();
                return result;
            } catch (Exception ex)
            {
                return ex.Message;
            }
            
        }
    }
}
