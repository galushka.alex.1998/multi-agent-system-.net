﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication;
using GNetic.Agents.agent.communication.amqp;
using GNetic.Agents.agent.communication.events;
using GNetic.Agents.agent.communication.events.models;
using GNetic.Agents.agent.communication.telegram;
using GNetic.Agents.UserAgent.layers;
using GNetic.Lib.logger;
using LightInject;

namespace GNetic.Agents.UserAgent
{
    public class UserAgentStartup : AgentStartup
    {
        public override void Configure(IServiceContainer container, IAgentInitialConfiguration configuration)
        {
            base.Configure(container, configuration);

            container.RegisterSingleton<ITelegramPeer, TelegramPeer>();

            container.RegisterSingleton<ITelegramPipelineHandler, UserSensorLayer>();
            container.RegisterSingleton<IRequestHandler<SolutionFoundRequest, SolutionFoundResponse>, ClusterSensorLayer>();

            container.RegisterMultiple<IAgentContext, IUserAgentContext, UserAgentContext>();
            container.RegisterSingleton<IClusterActuatorLayer, ClusterActuatorLayer>();
            container.RegisterSingleton<IUserActuatorLayer, UserActuatorLayer>();

            IUserAgentContext context = container.GetInstance<IUserAgentContext>();
            SetupContext(context, configuration);
            IAmqpPeer amqp = container.GetInstance<IAmqpPeer>();
            SetupAmqp(amqp, configuration);
            ITelegramPeer telegram = container.GetInstance<ITelegramPeer>();
            SetupTelegramApi(telegram, configuration);
            ILogger logger = container.GetInstance<ILogger>();
            SetupLogger(logger, configuration);
        }

        private void SetupContext(IUserAgentContext context, IAgentInitialConfiguration configuration)
        {
            TelegramConfig config = configuration.GetConfig<TelegramConfig>();
            context.UserName = config.AuthorizedUsername;
        }

        private void SetupAmqp(IAmqpPeer amqp, IAgentInitialConfiguration configuration)
        {
            AmqpConfig config = configuration.GetConfig<AmqpConfig>();
            amqp.Configure(config);
            amqp.Subscribe<AgentActivationEvent>();
            amqp.Subscribe<AgentDeactivationEvent>();
            amqp.Subscribe<SolutionFoundRequest, SolutionFoundResponse>();
        }

        private void SetupTelegramApi(ITelegramPeer telegram, IAgentInitialConfiguration configuration)
        {
            TelegramConfig telegramConfig = configuration.GetConfig<TelegramConfig>();
            telegram.Configure(telegramConfig);
        }

        private void SetupLogger(ILogger logger, IAgentInitialConfiguration configuration)
        {
            LoggerConfig loggerConfig = configuration.GetConfig<LoggerConfig>();
            logger.Configure(loggerConfig);
        }
    }
}
