﻿using GNetic.Agents.agent;

namespace GNetic.Agents.UserAgent
{
    public interface IUserAgentContext : IAgentContext
    {
        int UserName { get; set; }
    }
    public class UserAgentContext : AgentContext, IUserAgentContext
    {
        public int UserName { get; set; }
    }
}
