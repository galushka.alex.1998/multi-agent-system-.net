﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication;
using LightInject;
using GNetic.Agents.agent.layers;

namespace GNetic.Agents.UserAgent
{
    public class UserAgent : Agent
    {
        public override void Run()
        {
            IAmqpPeer amqp = Container.GetInstance<IAmqpPeer>();
            amqp.Run();
            amqp.AssertPublisher("all");
            IAgentContext context = Container.GetInstance<IAgentContext>();
            context.AgentInfo = new AgentInfo(amqp.QueueId, AgentType.UserAgent);
            IEnvironmentActuatorLayer actuatorLayer = Container.GetInstance<IEnvironmentActuatorLayer>();
            actuatorLayer.SendActivationSignal();
            ITelegramPeer telegram = Container.GetInstance<ITelegramPeer>();
            telegram.Run();
        }

        public override void Stop()
        {
            IAmqpPeer amqp = Container.GetInstance<IAmqpPeer>();
            ITelegramPeer telegram = Container.GetInstance<ITelegramPeer>();
            IEnvironmentActuatorLayer actuator = Container.GetInstance<IEnvironmentActuatorLayer>();
            actuator.SendDeactivationSignal();
            telegram.Stop();
            amqp.Stop();
        }
    }
}
