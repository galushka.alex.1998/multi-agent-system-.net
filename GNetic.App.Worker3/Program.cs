﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GNetic.App.Worker3
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkerApp app = new WorkerApp();
            app.Run("D:\\agents-root\\worker3-root");
            Thread.Sleep(Timeout.Infinite);
        }
    }
}
