﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication.amqp;
using GNetic.Agents.agent.communication.telegram;
using GNetic.Agents.UserAgent;
using GNetic.Lib.common;
using GNetic.Lib.logger;
using System.IO;

namespace GNetic.App
{
    public class UserAgentApp : IAgentApp<UserAgent>
    {
        public UserAgent Agent { get; set; }

        public void Run(string rootDir)
        {
            IAgentInitialConfiguration agentConfig = new AgentInitialConfiguration();

            LoggerConfig loggerConfig = File.ReadAllText($"{rootDir}\\logger\\config.json").ToModel<LoggerConfig>();
            TelegramConfig telegramConfig = File.ReadAllText($"{rootDir}\\telegram\\config.json").ToModel<TelegramConfig>();
            AmqpConfig amqpConfig = File.ReadAllText($"{rootDir}\\amqp\\config.json").ToModel<AmqpConfig>();

            agentConfig.SetConfig(loggerConfig);
            agentConfig.SetConfig(telegramConfig);
            agentConfig.SetConfig(amqpConfig);

            Agent = new UserAgent();
            Agent.Configure<UserAgentStartup>(agentConfig);
            Agent.Run();
        }
        public void Stop()
        {
            Agent.Stop();
        }
    }
}
