﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication.amqp;
using GNetic.Agents.WorkerAgent;
using GNetic.Lib.common;
using GNetic.Lib.logger;
using System.IO;
using System.Threading;

namespace GNetic.App
{
    public class WorkerApp : IAgentApp<WorkerAgent>
    {
        public WorkerAgent Agent { get; set; }
        public void Run(string rootDir)
        {
            Thread.Sleep(1000);
            IAgentInitialConfiguration agentConfig = new AgentInitialConfiguration();
            LoggerConfig loggerConfig = File.ReadAllText($"{rootDir}\\logger\\config.json").ToModel<LoggerConfig>();
            AmqpConfig amqpConfig = File.ReadAllText($"{rootDir}\\amqp\\config.json").ToModel<AmqpConfig>();
            agentConfig.SetConfig(loggerConfig);
            agentConfig.SetConfig(amqpConfig);

            Agent = new WorkerAgent();
            Agent.Configure<WorkerAgentStartup>(agentConfig);
            Agent.Run();
        }

        public void Stop()
        {
            Agent.Stop();
        }
    }
}
