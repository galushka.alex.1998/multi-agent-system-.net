﻿using GNetic.Agents.agent;

namespace GNetic.App
{
    public interface IAgentApp<TAgent> where TAgent : IAgent
    {
        TAgent Agent { get; set; }
        void Run(string rootDir);
        void Stop();
    }
}
