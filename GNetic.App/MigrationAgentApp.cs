﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication.amqp;
using GNetic.Agents.MigrationAgent;
using GNetic.Lib.common;
using GNetic.Lib.logger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.App
{
    public class MigrationAgentApp : IAgentApp<MigrationAgent>
    {
        public MigrationAgent Agent { get; set; }

        public void Run(string rootDir)
        {
            IAgentInitialConfiguration agentConfig = new AgentInitialConfiguration();
            LoggerConfig loggerConfig = File.ReadAllText($"{rootDir}\\logger\\config.json").ToModel<LoggerConfig>();
            AmqpConfig amqpConfig = File.ReadAllText($"{rootDir}\\amqp\\config.json").ToModel<AmqpConfig>();

            agentConfig.SetConfig(loggerConfig);
            agentConfig.SetConfig(amqpConfig);

            Agent = new MigrationAgent();
            Agent.Configure<MigrationAgentStartup>(agentConfig);
            Agent.Run();
        }

        public void Stop()
        {
            Agent.Stop();
        }
    }
}
