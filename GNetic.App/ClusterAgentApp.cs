﻿using GNetic.Agents.agent;
using GNetic.Agents.agent.communication.amqp;
using GNetic.Agents.ClusterAgent;
using GNetic.Agents.ClusterAgent.models;
using GNetic.Agents.WorkerAgent;
using GNetic.Lib.common;
using GNetic.Lib.logger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.App
{
    public class ClusterAgentApp : IAgentApp<ClusterAgent>
    {
        public ClusterAgent Agent { get; set; }
        public void Run(string rootDir)
        {
            IAgentInitialConfiguration agentConfig = new AgentInitialConfiguration();
            LoggerConfig loggerConfig = File.ReadAllText($"{rootDir}\\logger\\config.json").ToModel<LoggerConfig>();
            AmqpConfig amqpConfig = File.ReadAllText($"{rootDir}\\amqp\\config.json").ToModel<AmqpConfig>();
            AgentsConfiguration workerConfigs = File.ReadAllText($"{rootDir}\\configs\\config.json").ToModel<AgentsConfiguration>();
            agentConfig.SetConfig(loggerConfig);
            agentConfig.SetConfig(amqpConfig);
            agentConfig.SetConfig(workerConfigs);

            Agent = new ClusterAgent();
            Agent.Configure<ClusterAgentStartup>(agentConfig);
            Agent.Run();
        }

        public void Stop()
        {
            Agent.Stop();
        }
    }
}
