﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.dtos
{
    [Serializable]
    public class MathSettings
    {
        public double MathExpectationMax { get; set; }
        public double MathExpectationMin { get; set; }
        public double DeviationMax { get; set; }
        public double DeviationMin { get; set; }
    }
}
