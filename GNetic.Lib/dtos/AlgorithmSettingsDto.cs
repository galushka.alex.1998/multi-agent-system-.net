﻿using GNetic.Lib.gnetic.crossover;
using GNetic.Lib.gnetic.mutators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.dtos
{
    [Serializable]
    public class AlgorithmSettingsDto
    {
        public int InitialPopulationSize { get; set; }
        public int SurviveChromosomeCount { get; set; }
        public int CrossoveringChance { get; set; }
        public CrossoverType CrossoverType { get; set; }
        public int MutationChance { get; set; }
        public MutationType MutationType { get; set; }
        public int OldestChromosomeAge { get; set; }
        public int MaxOldestPercent { get; set; }

    }
}
