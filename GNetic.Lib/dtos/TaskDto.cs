﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.dtos
{
    [Serializable]
    public class TaskDto
    {
        public double[] TimeSeries { get; set; }
        public double Accuracy { get; set; }
    }
}
