﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.dtos
{
    [Serializable]
    public class WorkerTaskDto : WorkerSettingsDto
    {
        public TaskDto TaskDescription { get; set; }
    }
}
