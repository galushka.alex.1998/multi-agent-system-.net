﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.dtos
{
    [Serializable]
    public class LSystemSettingsDto
    {
        public int AxiomLengthMax { get; set; }
        public int AxiomLengthMin { get; set; }
        public int RuleCountMax { get; set; }
        public int RuleCountMin { get; set; }
        public int RuleLengthMax { get; set; }
        public int RuleLengthMin { get; set; }
    }
}
