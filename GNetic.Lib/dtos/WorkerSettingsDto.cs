﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.dtos
{
    [Serializable]
    public class WorkerSettingsDto
    {
        public AlgorithmSettingsDto AlgorithmSettings { get; set; }
        public LSystemSettingsDto LSystemSettings { get; set; }
        public MathSettings MathSettings { get; set; }
    }
}
