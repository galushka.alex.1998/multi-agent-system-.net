﻿using GNetic.Lib.common;
using GNetic.Lib.gnetic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.dtos
{
    [Serializable]
    public class SolutionDto
    {
        public double MathExpectation { get; set; }
        public double Deviation { get; set; }
        public double Fitness { get; set; }
        public IEnumerable<RuleDto> LSystem { get; set; }
        public TimeSpan? CalculationTime { get; set; } = null; 

        public static SolutionDto FromChromosome(Chromosome chromosome)
        {
            List<RuleDto> rules = new List<RuleDto>();
            rules.Add(new RuleDto
            {
                Axiom = chromosome.LSystem.AxiomRule.Axiom,
                Production = chromosome.LSystem.AxiomRule.Production.Join()
            });
            foreach(var rule in chromosome.LSystem.Rules)
            {
                rules.Add(new RuleDto
                {
                    Axiom = rule.Axiom,
                    Production = rule.Production.Join()
                });
            }
            return new SolutionDto
            {
                Deviation = chromosome.Deviation,
                LSystem = rules,
                Fitness = chromosome.Fitness,
                MathExpectation = chromosome.MathExpectation
            };
        }
    }

    [Serializable]
    public class RuleDto
    {
        public string Axiom { get; set; }
        public string Production { get; set; }
    }
}
