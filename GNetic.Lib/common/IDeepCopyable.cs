﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.common
{
    public interface IDeepCopyable<TEntity>
    {
        TEntity DeepCopy();
    }
}
