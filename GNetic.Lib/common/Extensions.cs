﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.common
{
    public static class ArrayExtensions
    {
        public static string Join(this string[] array)
        {
            return string.Join("", array);
        }
        public static string Join(this IEnumerable<string> array)
        {
            return string.Join("", array);
        }

        public static string Join(this char[] array)
        {
            return string.Join("", array);
        }
        public static string Join(this IEnumerable<char> array)
        {
            return string.Join("", array);
        }
        public static bool ContainsSomeOf(this string target, IEnumerable<string> statements)
        {
            return statements.Where(statement => target.Contains(statement)).Count() > 0;
        }
        public static string[] ToStringArray(this string source)
        {
            return source.ToArray().Select(ch => ch.ToString()).ToArray();
        }

        public static void AddRange<T>(this HashSet<T> hashSet, IEnumerable<T> values)
        {
            foreach(T value in values)
            {
                hashSet.Add(value);
            }
        }

        private static Random rng = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void Shuffle<T>(this IList<T> list, int from, int to)
        {
            IList<T> shuffleRange = new List<T>();
            for(int i = from; i <= to; i++)
            {
                shuffleRange.Add(list[i]);
            }
            shuffleRange.Shuffle();
            IList<T> resultRange = new List<T>();
            for(int i = 0; i < from; i++)
            {
                resultRange.Add(list[i]);
            }
            foreach(T item in shuffleRange)
            {
                resultRange.Add(item);
            }
            for(int i = to + 1; i < list.Count; i++)
            {
                resultRange.Add(list[i]);
            }
            for(int i = 0; i < list.Count; i++)
            {
                list[i] = resultRange[i];
            }
        }

        public static string ToJson(this object obj)
        {
            string json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            return json;
        }

        public static TModel ToModel<TModel>(this string json)
        {
            return JsonConvert.DeserializeObject<TModel>(json);
        }
    }

    
}
