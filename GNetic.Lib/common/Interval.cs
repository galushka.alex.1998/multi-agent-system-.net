﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.common
{
    public class Interval<T> : IDeepCopyable<Interval<T>> where T : struct, IComparable<T>
    {
        public T Min { get; set; } 
        public T Max { get; set; }

        public Interval(T min, T max)
        {
            Min = min;
            Max = max;
        }
        public Interval()
        {
            Min = default;
            Max = default;
        }

        public Interval<T> DeepCopy() =>
            new Interval<T>(Min, Max);

        public override string ToString()
        {
            return $"[Min: {Min}, Max: {Max}]";
        }
    }
}
