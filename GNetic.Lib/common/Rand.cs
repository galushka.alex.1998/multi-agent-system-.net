﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.common
{
    public class Rand
    {
        private static Random generator { get; set; } = new Random();

        public static double RandDouble(double min, double max)
        {
            return generator.NextDouble() * (max - min) + min;
        }

        public static double RandDouble(double min, double max, params double[] exceptValues)
        {
            double targetValue = 0;
            do
            {
                targetValue = RandDouble(min, max);
            } while (exceptValues.Contains(targetValue));
            return targetValue;
        }
        public static int RandInt(int min, int max)
        {
            return generator.Next(min, max + 1);
        }
        public static int RandInt(int min, int max, params int[] exceptValues)
        {
            int targetValue = 0;
            do
            {
                targetValue = RandInt(min, max);
            } while (exceptValues.Contains(targetValue));
            return targetValue;
        }
        public static T RandItem<T>(IEnumerable<T> array)
        {
            int index = RandIndex(array);
            return array.ElementAt(index);
        }

        public static int RandIndex<T>(IEnumerable<T> array)
        {
            int index = RandInt(0, array.Count() - 1);
            return index;
        }

        public static bool Truthy()
        {
            return RandInt(0, 1) == 0;
        }
        



    }

}
