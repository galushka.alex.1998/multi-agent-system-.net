﻿using GNetic.Lib.common;
using GNetic.Lib.gmatic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.builders
{
    public class GraphBuilder : IBuilder<Node, LSystem>
    {
        public Node Build(LSystem options)
        {
            LSystem system = options;

            HashSet<string> usedLexemsSet = new HashSet<string>();
            usedLexemsSet.Add(system.AxiomRule.Axiom);
            usedLexemsSet.AddRange(system.AxiomRule.Production);
            foreach (Rule rule in system.Rules)
            {
                usedLexemsSet.Add(rule.Axiom);
                usedLexemsSet.AddRange(rule.Production);
            }
            List<Node> nodes = usedLexemsSet.Select(symbol => new Node(symbol)).ToList();

            List<Rule> rules = new List<Rule>();
            rules.Add(system.AxiomRule.DeepCopy());
            foreach(Rule rule in system.Rules)
            {
                rules.Add(rule.DeepCopy());
            }

            foreach(Rule rule in rules)
            {
                Node node = nodes.Find(n => n.Axiom == rule.Axiom);
                foreach(string symbol in rule.Production)
                {
                    Node child = nodes.FirstOrDefault(n => n.Axiom == symbol);
                    if (child != null)
                    {
                        node.RelatedNodes.Add(child);
                    }
                }
            }
            return nodes.First();
        }
    }
}
