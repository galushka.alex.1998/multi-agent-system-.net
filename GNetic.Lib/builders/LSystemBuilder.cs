﻿using GNetic.Lib.common;
using GNetic.Lib.gmatic;
using System.Collections.Generic;
using System.Linq;

namespace GNetic.Lib.builders
{

    public class LSystemBuilder : IBuilder<LSystem, Constraints>
    {
        public LSystem Build(Constraints options)
        {
            Alphabet sourceAlphabet = new Alphabet();
            Constraints constraints = options;

            LSystem system = null;
            do
            {
                system = new LSystem();
                int ruleCount = Rand.RandInt(constraints.RuleCount.Min, constraints.RuleCount.Max);
                int axiomLength = Rand.RandInt(constraints.AxiomLength.Min, constraints.AxiomLength.Max);
                string[] significantLexems = sourceAlphabet.SignificantLiterals.Take(ruleCount).ToArray();
                Rule axiom = GenerateRule(sourceAlphabet.StartSymbol, significantLexems, new string[] { }, axiomLength); 
                system.AddAxiom(sourceAlphabet.StartSymbol, axiom.Production);

                for (int i = 0; i < ruleCount; i++)
                {
                    string ruleAxiom = significantLexems[i];
                    string lexem = significantLexems[i < ruleCount - 1 ? i + 1 : 0];
                    int ruleLength = Rand.RandInt(constraints.RuleLength.Min, constraints.RuleLength.Max);
                    Rule rule = GenerateRule(ruleAxiom, new string[] { lexem }, sourceAlphabet.FunctorLiterals, ruleLength);
                    system.AddRule(rule);
                }

            } while (!Validator.IsValid(system, sourceAlphabet));
            return system;
        }
        private static Rule GenerateRule(string axiom, string[] significantLexems, string[] functorLexems, int ruleLength)
        {
            List<string> composeLiterals = new List<string>();
            composeLiterals.AddRange(significantLexems);
            composeLiterals.AddRange(functorLexems);
            composeLiterals.Add(string.Empty);

            Rule rule;
            do
            {
                List<string> production = new List<string>();
                for (int i = 0; i < ruleLength; i++)
                {
                    production.Add(Rand.RandItem(composeLiterals));
                }
                string stringProduction = production.Join();

                //string[] unnecessaryStatements = new string[] { "+-", "-+" };
                //while (stringProduction.ContainsSomeOf(unnecessaryStatements))
                //{
                //    for (int i = 0; i < unnecessaryStatements.Length; i++)
                //    {
                //        string statement = unnecessaryStatements[i];
                //        stringProduction = stringProduction.Replace(statement, string.Empty);
                //    }
                //}
                rule = new Rule(axiom, stringProduction.ToStringArray());
            } while (rule.Production.Length < 1 && rule.Production.Where(s => s != "+" && s != "-").Count() > 0);
            return rule;
        }
    }
}
