﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.builders
{
    public interface IBuilder <out TConstruction, in TOptions>
    {
        TConstruction Build(TOptions options);
    }
}
