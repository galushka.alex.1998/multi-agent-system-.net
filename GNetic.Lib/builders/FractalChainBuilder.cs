﻿using GNetic.Lib.common;
using GNetic.Lib.gmatic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.builders
{
    public class FractalChainBuildOptions
    {
        public int SignificantSymbolsCount { get; set; }
        public LSystem LSystem { get; set; }
        public Alphabet Alphabet { get; set; }

        public FractalChainBuildOptions(LSystem system, int significantSymbolsCount, Alphabet alphabet = null)
        {
            LSystem = system;
            SignificantSymbolsCount = significantSymbolsCount;
            Alphabet = alphabet ?? new Alphabet();
        }
    }
    public class FractalChainBuilder : IBuilder<string, FractalChainBuildOptions>
    {
        public string Build(FractalChainBuildOptions options)
        {
            string start = options.LSystem.AxiomRule.Production.Join();
            while (start.ToStringArray().Where(lexem => options.Alphabet.SignificantLiterals.Contains(lexem)).Count() < options.SignificantSymbolsCount)
            {
                start = options.LSystem.Iterate(start);
            }

            int significantCount = 0;
            string result = start.TakeWhile((symbol) =>
            {
                if (options.Alphabet.SignificantLiterals.ToList().Contains(symbol.ToString()))
                {
                    significantCount++;
                }
                return significantCount <= options.SignificantSymbolsCount;
            }).Join();
            return result;
        }
    }
}
