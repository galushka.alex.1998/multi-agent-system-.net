﻿using GNetic.Lib.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.builders
{
    public class FractalSeriesBuildOptions
    {
        public string Sequence { get; set; }
        public double MathExpectation { get; set; }
        public double Deviation { get; set; }
        public FractalSeriesBuildOptions(string sequence, double mathExpectation, double deviation)
        {
            Sequence = sequence;
            MathExpectation = mathExpectation;
            Deviation = deviation;
        }
    }
    public class FractalSeriesBuilder : IBuilder<double[], FractalSeriesBuildOptions>
    {
        public double[] Build(FractalSeriesBuildOptions options)
        {
            List<double> timeSeries = new List<double>();

            double mathExpectation = options.MathExpectation;
            foreach (var lexem in options.Sequence.ToStringArray())
            {
                switch (lexem)
                {
                    case "f":
                    case "g":
                    case "h":
                    case "p":
                        timeSeries.Add(mathExpectation);
                        break;
                    case "+":
                        mathExpectation += options.Deviation;
                        break;
                    case "-":
                        mathExpectation -= options.Deviation;
                        break;
                }
            }
            return timeSeries.ToArray();
        }

    }
}
