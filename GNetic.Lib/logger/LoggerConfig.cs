﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.logger
{
    [JsonObject]
    public class LoggerConfig
    {
        [JsonProperty("loggers")]
        public IEnumerable<ConfigLogger> Loggers;
    }

    [JsonObject]
    public class ConfigLogger
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("outputDirectory")]
        public string OutputDirectory { get; set; }
    }
}
