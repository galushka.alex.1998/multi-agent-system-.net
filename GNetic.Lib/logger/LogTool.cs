﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.logger
{
    public class LogTool
    {
        private ILogTransport LogTransport { get; set; }
        public LogTool(ILogTransport logTransport)
        {
            LogTransport = logTransport;
        }

        public void Log(string message)
        {
            LogTransport.Log(message);
        }
    }
}
