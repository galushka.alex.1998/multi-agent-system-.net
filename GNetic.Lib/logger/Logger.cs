﻿using GNetic.Lib.common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.logger
{
    public interface ILogger
    {
        void Log(params object[] args);
        void Configure(LoggerConfig config);
    }

    public class Logger : ILogger, IConfigurable<LoggerConfig>
    {
        private IList<LogTool> LogTools = new List<LogTool>();

        public void Configure(LoggerConfig config)
        {
            LogTools.Clear();
            if (config.Loggers == null || config.Loggers.Count() == 0)
                throw new Exception("Loggers not presented");
            foreach(ConfigLogger loggerInstance in config.Loggers)
            {
                switch (loggerInstance.Type)
                {
                    case "console":
                        ILogTransport consoleTransport = new ConsoleLogger();
                        LogTool console = new LogTool(consoleTransport);
                        LogTools.Add(console);
                        break;
                    case "file":
                        string dir = loggerInstance.OutputDirectory;
                        if (string.IsNullOrEmpty(dir))
                            throw new Exception("logger output directory is not valid");
                        if (!Directory.Exists(dir))
                            Directory.CreateDirectory(dir);
                        ILogTransport fileTransport = new FileLogger($"{Guid.NewGuid()}.log");
                        LogTool fileLogger = new LogTool(fileTransport);
                        LogTools.Add(fileLogger);
                        break;
                    default:
                        throw new Exception("Unknown logger format");
                }
            }
        }

        public void Log(params object[] objs)
        {
            foreach (LogTool tool in LogTools)
            {
                string message = RawToString(objs);
                string formatted = FormatMessage(message);
                tool.Log(formatted);
            }
        }

        private string FormatMessage(string message)
        {
            return $"[{DateTime.Now}] {message}";
        }
        private string RawToString(params object[] objs)
        {
            return string.Join(" ", objs.Select(obj => $"[{obj}]"));
        }
    }
    
   
}
