﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.logger
{
    public class ConsoleLogger : ILogTransport
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
