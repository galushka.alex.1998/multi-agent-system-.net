﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.logger
{
    public class FileLogger : ILogTransport, IDisposable
    {
        private StreamWriter FileWriter { get; set; }
        public FileLogger(string filePath)
        {
            FileWriter = new StreamWriter(filePath, true);
        }
        public void Log(string message)
        {
            FileWriter.WriteLineAsync(message);
        }

        public void Dispose()
        {
            FileWriter.Dispose();
        }
    }
}
