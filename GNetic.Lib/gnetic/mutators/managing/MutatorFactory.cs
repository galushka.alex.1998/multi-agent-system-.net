﻿using GNetic.Lib.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.gnetic.mutators
{
    public class MutatorFactory
    {
        private Dictionary<MutationType, IMutator> mutators = new Dictionary<MutationType, IMutator>();
        public MutatorFactory()
        {
            mutators.Add(MutationType.RandomCreator, new RandomMutator());
            mutators.Add(MutationType.FractalConstraintsInherits, new FractalInheritanceMutator());
            mutators.Add(MutationType.Shuffler, new ShuffleMutator());
        }

        public IMutator GetMutatorByType(MutationType type)
        {
            return mutators[type];
        }
    }
}
