﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.gnetic.mutators
{
    public interface IMutator
    {
        Chromosome Mutate(Chromosome chromosome);
    }
}
