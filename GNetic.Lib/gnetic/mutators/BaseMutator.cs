﻿using GNetic.Lib.common;
using GNetic.Lib.gmatic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.gnetic.mutators
{
    public class BaseMutator : IMutator
    {
        public Chromosome Mutate(Chromosome chromosome)
        {
            int randomGen = Rand.RandInt(0, 2);
            Chromosome mutant = chromosome.DeepCopy();
            switch (randomGen)
            {
                case 0:
                    mutant.LSystem = MutateFractal(chromosome);
                    break;
                case 1:
                    mutant.MathExpectation = MutateExpectation(chromosome);
                    break;
                case 2:
                    mutant.Deviation = MutateDeviation(chromosome);
                    break;
            }
            return mutant;
        }

        protected virtual LSystem MutateFractal(Chromosome chromosome)
        {
            return chromosome.LSystem.DeepCopy();
        }

        protected virtual double MutateExpectation(Chromosome chromosome)
        {
            return Rand.RandDouble(chromosome.Options.MathExpectation.Min, chromosome.Options.MathExpectation.Max);
        }
        protected virtual double MutateDeviation(Chromosome chromosome)
        {
            return Rand.RandDouble(chromosome.Options.Deviation.Min, chromosome.Options.Deviation.Max);
        }
    }
}
