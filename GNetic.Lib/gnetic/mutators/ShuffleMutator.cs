﻿using GNetic.Lib.common;
using GNetic.Lib.gmatic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.gnetic.mutators
{
    public class ShuffleMutator : BaseMutator
    {
        protected override LSystem MutateFractal(Chromosome chromosome)
        {
            LSystem system = chromosome.LSystem.DeepCopy();
            int ruleCount = chromosome.LSystem.Rules.Count;
            
            for (int i = 0; i < ruleCount; i++)
            {
                Rule rule = system.Rules[i];
                rule.Production.Shuffle();
            }
            return system;
        }
    }
}
