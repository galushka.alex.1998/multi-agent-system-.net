﻿using GNetic.Lib.builders;
using GNetic.Lib.gmatic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.gnetic.mutators
{
    public class RandomMutator : BaseMutator
    {
        protected override LSystem MutateFractal(Chromosome chromosome)
        {
            LSystemBuilder builder = new LSystemBuilder();
            LSystem system = builder.Build(chromosome.Options.LSystemConstraints);
            return system;
        }
    }
}
