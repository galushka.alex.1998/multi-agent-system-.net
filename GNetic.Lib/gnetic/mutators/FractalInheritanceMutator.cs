﻿using GNetic.Lib.gmatic;
using GNetic.Lib.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNetic.Lib.builders;

namespace GNetic.Lib.gnetic.mutators
{
    public class FractalInheritanceMutator : BaseMutator
    {
        protected override LSystem MutateFractal(Chromosome chromosome)
        {
            LSystemStats systemStats = chromosome.LSystem.GetStats();
            Constraints constraints = new Constraints()
            {
                AxiomLength = new Interval<int>(systemStats.AxiomLength, systemStats.AxiomLength),
                RuleCount = new Interval<int>(systemStats.RuleCount, systemStats.RuleCount),
                RuleLength = chromosome.Options.LSystemConstraints.RuleLength.DeepCopy()
            };
            LSystemBuilder builder = new LSystemBuilder();
            LSystem system = builder.Build(constraints);
            return system;
        }
    }
}
