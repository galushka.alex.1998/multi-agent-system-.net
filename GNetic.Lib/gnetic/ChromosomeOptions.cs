﻿using GNetic.Lib.common;
using GNetic.Lib.gmatic;
using GNetic.Lib.gnetic.crossover;
using GNetic.Lib.gnetic.mutators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.gnetic
{
    public class ChromosomeOptions : IDeepCopyable<ChromosomeOptions>
    {
        public Interval<double> MathExpectation { get; set; }
        public Interval<double> Deviation { get; set; }
        public Constraints LSystemConstraints { get; set; }
        public MutationType MutationType { get; set; }
        public CrossoverType CrossoverType { get; set; }
        public ChromosomeOptions DeepCopy() =>
            new ChromosomeOptions()
            {
                MathExpectation = MathExpectation,
                Deviation = Deviation,
                LSystemConstraints = LSystemConstraints.DeepCopy(),
                MutationType = MutationType,
                CrossoverType = CrossoverType
            };
    }
}
