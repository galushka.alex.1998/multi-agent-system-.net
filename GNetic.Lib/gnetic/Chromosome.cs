﻿using GNetic.Lib.builders;
using GNetic.Lib.common;
using GNetic.Lib.gmatic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.gnetic
{
    public class Chromosome : IDeepCopyable<Chromosome>
    {
        public ChromosomeOptions Options { get; private set; }
        public TaskOptions TaskOptions { get; private set; }
        public Guid Id { get; set; } = Guid.NewGuid();
        public int Age { get; set; }
        public double Fitness { get; set; }
        public LSystem LSystem { get; set; }
        public double MathExpectation { get; set; }
        public double Deviation { get; set; }
        public Chromosome(ChromosomeOptions options, TaskOptions taskOptions)
        {
            Options = options;
            TaskOptions = taskOptions;
            Id = Guid.NewGuid();
        }
        public void MakeBase()
        {
            LSystem = MakeLSystem();
            MathExpectation = Rand.RandDouble(Options.MathExpectation.Min, Options.MathExpectation.Max);
            Deviation = Rand.RandDouble(Options.Deviation.Min, Options.Deviation.Max);
            CalculateFitness();
        }
        public Chromosome DeepCopy() =>
            new Chromosome(Options, TaskOptions)
            {
                Age = 0,
                Fitness = Fitness,
                LSystem = LSystem.DeepCopy(),
                MathExpectation = MathExpectation,
                Deviation = Deviation
            };

        public void CalculateFitness()
        {

            FractalChainBuilder chainBuilder = new FractalChainBuilder();
            FractalChainBuildOptions chainBuildOptions = new FractalChainBuildOptions(LSystem, TaskOptions.TimeSeries.Length);
            string sequence = chainBuilder.Build(chainBuildOptions);

            FractalSeriesBuilder seriesBuilder = new FractalSeriesBuilder();
            FractalSeriesBuildOptions seriesBuildOptions = new FractalSeriesBuildOptions(sequence, MathExpectation, Deviation);
            double[] timeSeries = seriesBuilder.Build(seriesBuildOptions);

            double fitness = 0;
            for(int i = 0; i < TaskOptions.TimeSeries.Length; i++)
            {
                fitness += Math.Pow(TaskOptions.TimeSeries[i] - timeSeries[i], 2);
            }
            Fitness = fitness;
        }

        private LSystem MakeLSystem()
        {
            LSystemBuilder builder = new LSystemBuilder();
            return builder.Build(Options.LSystemConstraints);
        }

        public override string ToString()
        {
            return $"{LSystem} {MathExpectation} {Deviation}";
        }

    }
}
