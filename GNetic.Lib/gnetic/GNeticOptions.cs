﻿using GNetic.Lib.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.gnetic
{
    public class GNeticOptions
    {   
        public int InitialPopulationSize { get; set; }
        public int SurviveChromosomeCount { get; set; }
        public int CrossoveringChance { get; set; }
        public int MutationChance { get; set; }
        public int OldestChromosomeAge { get; set; }
        public int MaxOldestPercent { get; set; }
    }
}
