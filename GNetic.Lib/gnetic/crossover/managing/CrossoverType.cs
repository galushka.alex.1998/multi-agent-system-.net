﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.gnetic.crossover
{
    public enum CrossoverType
    {
        FractalDeviationInheritance,
        FractalExpectationInheritance,
        ExpectationDeviationInheritance
    }
}
