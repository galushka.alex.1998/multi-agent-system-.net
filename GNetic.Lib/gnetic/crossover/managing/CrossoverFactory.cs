﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.gnetic.crossover.managing
{
    public class CrossoverFactory
    {
        private Dictionary<CrossoverType, ICrossover> crossovers = new Dictionary<CrossoverType, ICrossover>();
        public CrossoverFactory()
        {
            crossovers.Add(CrossoverType.ExpectationDeviationInheritance, new ExpectationDeviationCrossover());
            crossovers.Add(CrossoverType.FractalDeviationInheritance, new FractalDeviationInheritanceCrossover());
            crossovers.Add(CrossoverType.FractalExpectationInheritance, new FractalDeviationInheritanceCrossover());
        }

        public ICrossover GetCrossoverByType(CrossoverType type)
        {
            return crossovers[type];
        }
    }
}
