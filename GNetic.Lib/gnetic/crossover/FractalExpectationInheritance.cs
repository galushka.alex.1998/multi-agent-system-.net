﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.gnetic.crossover
{
    public class FractalExpectationInheritance : ICrossover
    {
        public Chromosome Crossover(Chromosome mom, Chromosome dad)
        {
            Chromosome child = mom.DeepCopy();
            child.Deviation = dad.Deviation;
            return child;
        }
    }
}
