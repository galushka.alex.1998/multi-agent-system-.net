﻿using GNetic.Lib.common;
using GNetic.Lib.gnetic.crossover;
using GNetic.Lib.gnetic.crossover.managing;
using GNetic.Lib.gnetic.mutators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using GNetic.Lib.logger;

namespace GNetic.Lib.gnetic
{
    public class GNeticAlgorithm
    {
        private GNeticOptions Options { get; set; }
        private ChromosomeOptions ChromosomeOptions { get; set; }
        private TaskOptions TaskOptions { get; set; }
        private Thread WorkerThread { get; set; }
        private List<Chromosome> Population { get; set; }
        private int IterationsCount { get; set; } = 0;
        private bool IsInProcess { get; set; } = false;
        private DateTime StartedOn { get; set; } 
        private TimeSpan Durability { get; set; }
        private MutatorFactory MutatorFactory { get; set; } = new MutatorFactory();
        private CrossoverFactory CrossoverFactory { get; set; } = new CrossoverFactory();
        private List<Chromosome> ExternalChromosomes = new List<Chromosome>();
        private ILogger Logger { get; set; }
        public GNeticAlgorithm(TaskOptions taskOptions, GNeticOptions options, ChromosomeOptions chromosomeOptions, ILogger logger)
        {
            TaskOptions = taskOptions;
            Options = options;
            ChromosomeOptions = chromosomeOptions;
            Logger = logger;
        }

        public void ApplyExternalChromosomes(IEnumerable<Chromosome> chromosomes)
        {
            var chrs = chromosomes.Where(c => c != null);
            foreach (var chromosome in chrs)
                chromosome.Age = 0;
            Logger.Log($"Applying external chromosomes: {chrs.Select(c => c.ToString() + "\n").Join()}");
            ExternalChromosomes.AddRange(chrs);
        }

        public void ApplySettings(GNeticOptions options, ChromosomeOptions chromosomeOptions)
        {
            Options = options;
            ChromosomeOptions = chromosomeOptions;

            Logger.Log(Options.ToJson(), ChromosomeOptions.ToJson());
        }
        public void Run()
        {
            StartedOn = DateTime.Now;
            Thread workerThread = new Thread(new ThreadStart(Processor));
            WorkerThread = workerThread;
            IsInProcess = true;
            WorkerThread.Start();
            Logger.Log( 
                Options.ToJson(), 
                ChromosomeOptions.ToJson()
            );
        }

        public void Stop()
        {
            IsInProcess = false;
            Durability = DateTime.Now - StartedOn;
            Population = (from c in Population
                         orderby c.Fitness
                         select c).ToList();
            Chromosome bestChromosome = Population[0];
            Thread.Sleep(100);
            Logger.Log(
                bestChromosome, 
                $"Iterations count: {IterationsCount}", 
                $"Calculation time: {Durability}"
            );
        }

        public Chromosome GetBestChromosome()
        {
            Chromosome chromosome = (
                from c in Population 
                orderby c.Fitness 
                select c
            ).First();

            return chromosome;
        }

        private void Processor()
        {
            List<Chromosome> chromosomes = new List<Chromosome>();
            for(int i = 0; i < Options.InitialPopulationSize; i++)
            {
                Chromosome chromosome = new Chromosome(ChromosomeOptions, TaskOptions);
                chromosome.MakeBase();
                chromosomes.Add(chromosome);
            }
            Population = chromosomes;
            while (IsInProcess)
                PerformIteration();
        }

        private void PerformIteration()
        {
            RebasePopulationPhase();
            AddExternalChromosomes();
            CrossoverPhase();
            MutationPhase();
            CloneEliminationPhase();
            SelectionPhase();
        }

        private void AddExternalChromosomes()
        {
            if (ExternalChromosomes.Count == 0)
                return;
            Population.AddRange(ExternalChromosomes);
            ExternalChromosomes.Clear();
        }

        private void RebasePopulationPhase()
        {
            IEnumerable<Chromosome> oldestChromosomes = Population.Where(chromosome => chromosome.Age > Options.OldestChromosomeAge);
            int oldestCount = oldestChromosomes.Count();

            if (oldestCount >= Population.Count * Options.MaxOldestPercent / 100)
            {
                double averageAge = (from c in Population select c.Age).Sum() / Population.Count;
                Logger.Log($"Average chromosome age : {averageAge}");
                Population = (from c in Population
                             orderby c.Fitness
                             select c).ToList();
                Chromosome bestChromosome = Population.First();
                Population.Clear();
                Population.Add(bestChromosome);
                for (int i = 0; i < Options.InitialPopulationSize; i++)
                {
                    Chromosome chromosome = new Chromosome(ChromosomeOptions, TaskOptions);
                    chromosome.MakeBase();
                    Population.Add(chromosome);
                }
            }
        }
        private void CrossoverPhase()
        {

            IList<Chromosome> crossovering = Population.Where(c => Rand.RandInt(0, 100) <= Options.CrossoveringChance).ToList();
            crossovering.Shuffle();
            if (crossovering.Count % 2 != 0)
            {
                crossovering.RemoveAt(crossovering.Count - 1);
            }
            ICrossover crossoverOperator = CrossoverFactory.GetCrossoverByType(ChromosomeOptions.CrossoverType);
            for(int i = 0; i < crossovering.Count - 1; i++)
            {
                Chromosome parent1 = crossovering[i];
                Chromosome parent2 = crossovering[i + 1];
                Chromosome child1 = crossoverOperator.Crossover(parent1, parent2);
                Chromosome child2 = crossoverOperator.Crossover(parent2, parent1);
                child1.CalculateFitness();
                child2.CalculateFitness();
                Population.Add(child1);
                Population.Add(child2);
            }
        }
        private void MutationPhase()
        {
            IList<Chromosome> mutants = Population.Where(c => Rand.RandInt(0, 100) <= Options.MutationChance).ToArray();
            IMutator mutationOperator = MutatorFactory.GetMutatorByType(ChromosomeOptions.MutationType);
            foreach(Chromosome mutant in mutants)
            {
                Chromosome mutated = mutationOperator.Mutate(mutant);
                mutated.CalculateFitness();
                Population.Add(mutated);
            }
        }
        private void SelectionPhase()
        {
            Population = (
                from c in Population 
                orderby c.Fitness 
                select c
            ).Take(Options.SurviveChromosomeCount).ToList();
            foreach(Chromosome chromosome in Population)
            {
                chromosome.Age++;
            }
            IterationsCount++;
            var avgFitness = (
                from c in Population 
                select c.Fitness
            ).Sum() / Population.Count;

            Logger.Log(
                $"Iteration : {IterationsCount}",
                $"Average population fitness : {avgFitness}",
                $"Best fitness : {Population[0].Fitness}",
                $"Chromosome : {Population[0]}"
            );
        }
        private void CloneEliminationPhase()
        {
            var uniqPopulation = (
                from c in Population
                group c by c.ToString()
                into g
                where g.FirstOrDefault() != null
                select g.First()
            ).ToList();

            if (uniqPopulation.Count < Population.Count)
            {
                int diff = Population.Count - uniqPopulation.Count;
                for (int i = 0; i < diff; i++)
                {
                    Chromosome chromosome = new Chromosome(ChromosomeOptions, TaskOptions);
                    chromosome.MakeBase();
                    uniqPopulation.Add(chromosome);
                }
            }
            Population = uniqPopulation;
        }

        
    }
}
