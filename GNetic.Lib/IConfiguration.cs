﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib
{
    public interface IConfigurable<TConfig>
        where TConfig : class
    {
        void Configure(TConfig config);
    }

}
