﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNetic.Lib.gmatic
{
    public class LSystemStats
    {
        public int AxiomLength { get; set; }
        public int RuleCount { get; set; }
        public int AverageRuleLength { get; set; }
    }
}
