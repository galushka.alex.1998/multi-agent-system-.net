﻿using GNetic.Lib.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.gmatic
{
    public class Rule : IDeepCopyable<Rule>
    {
        public string Axiom { get; set; }
        public string[] Production { get; set; }

        public Rule(string axiom, string[] production)
        {
            Axiom = axiom;
            Production = production;
        }
        public Rule DeepCopy() =>
            new Rule(Axiom, Production.ToArray());
        public override string ToString() =>
            $"{Axiom} => {string.Join("", Production)}";
    }
}
