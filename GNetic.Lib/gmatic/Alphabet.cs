﻿using GNetic.Lib.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.gmatic
{
    public class Alphabet : IDeepCopyable<Alphabet>
    {
        public string StartSymbol { get; set; } = "s";
        public string[] SignificantLiterals { get; set; } = new string[] { "f", "g", "h", "p", "z","x","y" };
        public string[] FunctorLiterals { get; set; } = new string[] { "+", "-" };
        public Alphabet DeepCopy() =>
            new Alphabet()
            {
                StartSymbol = StartSymbol,
                SignificantLiterals = SignificantLiterals.ToArray(),
                FunctorLiterals = FunctorLiterals.ToArray(),
            };
    }
}
