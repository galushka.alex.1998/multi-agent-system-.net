﻿using GNetic.Lib.builders;
using GNetic.Lib.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.gmatic
{
    public class Validator
    {
        public static bool IsValid(LSystem system, Alphabet alphabet)
        {

            //return system.Rules.Where(r => r.Production.Where(s => s != "+" && s != "-").Count() > 1).Count() > 0;
            GraphBuilder graphBuilder = new GraphBuilder();

            HashSet<string> lexemSet = new HashSet<string>();
            lexemSet.Add(system.AxiomRule.Axiom);
            lexemSet.AddRange(system.AxiomRule.Production);
            foreach (Rule rule in system.Rules)
            {
                lexemSet.Add(rule.Axiom);
                lexemSet.AddRange(rule.Production);
            }

            bool lSystemContactivityResult = lexemSet.All(literal =>
            {
                Node graph = graphBuilder.Build(system);
                return graph.IsRelatedWith(literal);
            });


            bool isReproducing = true;
            string start = system.AxiomRule.Production.Join();
            for (int i = 0; i < system.Rules.Count; i++)
            {
                string production = system.Iterate(start);
                string significantProduction = production.Where(symbol => alphabet.SignificantLiterals.Contains(symbol.ToString())).Join();
                string significantStart = start.Where(symbol => alphabet.SignificantLiterals.Contains(symbol.ToString())).Join();
                if (significantProduction.Length <= significantStart.Length)
                {
                    isReproducing = false;
                    break;
                }
                start = production;
            }
            return lSystemContactivityResult && isReproducing;
        }

        
    }
}
