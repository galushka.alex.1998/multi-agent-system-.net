﻿using GNetic.Lib.common;
using GNetic.Lib.gnetic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.gmatic
{
    public class Constraints : IDeepCopyable<Constraints>
    {
        public Interval<int> AxiomLength { get; set; } = new Interval<int>();
        public Interval<int> RuleCount { get; set; } = new Interval<int>();
        public Interval<int> RuleLength { get; set; } = new Interval<int>();
        public void FillFromAlphabet(Alphabet alphabet)
        {
            AxiomLength.Min = 1;
            AxiomLength.Max = alphabet.SignificantLiterals.Length;
            RuleCount.Min = 1;
            RuleCount.Max = alphabet.SignificantLiterals.Length;
            RuleLength.Min = 2;
            RuleLength.Max = alphabet.SignificantLiterals.Length + alphabet.FunctorLiterals.Length;
        }
        public Constraints DeepCopy() =>
            new Constraints()
            {
                AxiomLength = AxiomLength.DeepCopy(),
                RuleCount = RuleCount.DeepCopy(),
                RuleLength = RuleLength.DeepCopy()
            };

    }
}
