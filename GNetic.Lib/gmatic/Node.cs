﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.gmatic
{
    public class Node
    {
        public string Axiom { get; set; }
        public HashSet<Node> RelatedNodes { get; set; } = new HashSet<Node>();
        private bool IsVisited { get; set; } = false;
        public Node(string axiom)
        {
            Axiom = axiom;
        }

        public bool IsRelatedWith(string relatedAxiom)
        {
            if (IsVisited)
            {
                return false;
            }

            IsVisited = true;

            Node targetNode = null;
            if (Axiom == relatedAxiom)
            {
                targetNode = this;
            }
            else
            {
                targetNode = RelatedNodes.FirstOrDefault(node => node.Axiom == relatedAxiom || node.IsRelatedWith(relatedAxiom));
            }
            return targetNode != null;
        }
    }
}
