﻿using GNetic.Lib.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.gmatic
{
    public class LSystem : IDeepCopyable<LSystem>
    {
        public Rule AxiomRule { get; set; }
        public List<Rule> Rules { get; private set; } = new List<Rule>();
        public void AddAxiom(string startSymbol, string[] axiom)
        {
            AxiomRule = new Rule(startSymbol, axiom);
        }
        public void AddRule(Rule rule)
        {
            Rules.Add(rule);
        }

        public void RemoveRule(string axiom)
        {
            Rules.RemoveAll(r => r.Axiom == axiom);
        }

        public string Iterate(string start = "")
        {
            string axiom = start != string.Empty ? start : AxiomRule.Production.Join();
            string iterationResult = string.Empty;
            foreach(string symbol in axiom.ToStringArray())
            {
                Rule targetRule = Rules.FirstOrDefault(rule => rule.Axiom == symbol);
                iterationResult += targetRule != null ? targetRule.Production.Join() : symbol;
            }
            return iterationResult;
        }

        public static LSystem FromSpecification(Specification spec)
        {
            LSystem lSystem = new LSystem();
            lSystem.AddAxiom(spec.Axiom.Axiom, spec.Axiom.Production);
            foreach(Rule rule in spec.Rules)
            {
                lSystem.AddRule(rule.DeepCopy());
            }
            return lSystem;
        }

        public Specification ToSpecification()
        {
            Specification specification = new Specification();
            specification.Axiom = AxiomRule.DeepCopy();
            foreach(Rule rule in Rules)
            {
                specification.Rules.Add(rule.DeepCopy());
            }
            return specification;
        }

        public LSystemStats GetStats() => new LSystemStats()
        {
            AxiomLength = AxiomRule.Production.Length,
            RuleCount = Rules.Count,
            AverageRuleLength = (from r in Rules select r.Production.Length).Sum()
        };
        public LSystem DeepCopy() =>
            FromSpecification(ToSpecification());

        public override string ToString()
        {
            List<string> result = new List<string>();
            result.Add(AxiomRule.Production.Join());
            foreach(Rule rule in Rules)
            {
                result.Add(rule.ToString());
            }
            return string.Join(" ", result);
        }
    }
}
