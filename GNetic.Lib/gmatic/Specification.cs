﻿using GNetic.Lib.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNetic.Lib.gmatic
{
    public class Specification : IDeepCopyable<Specification>
    {
        public Rule Axiom { get; set; }
        public List<Rule> Rules { get; set; } = new List<Rule>();
        public Specification DeepCopy() =>
            new Specification()
            {
                Axiom = Axiom.DeepCopy(),
                Rules = Rules.Select(rule => rule.DeepCopy()).ToList()
            };

    }

    
}
