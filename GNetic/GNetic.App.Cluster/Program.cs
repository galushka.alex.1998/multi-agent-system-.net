﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GNetic.App.Cluster
{
    class Program
    {
        static void Main(string[] args)
        {
            ClusterAgentApp app = new ClusterAgentApp();
            app.Run("D:\\agents-root\\cluster-root");
            Thread.Sleep(Timeout.Infinite);
        }
    }
}
