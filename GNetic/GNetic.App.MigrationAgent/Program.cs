﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GNetic.App.MigrationAgent
{
    class Program
    {
        static void Main(string[] args)
        {
            MigrationAgentApp app = new MigrationAgentApp();
            app.Run("D:\\agents-root\\migration-agent-root");
            Thread.Sleep(Timeout.Infinite);
        }
    }
}
